/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.daos;

import com.mycompany.cinemaproject.entities.Reservations;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author OKTATAS
 */
public interface ReservationDao extends JpaRepository<Reservations, Long> {
    
    List<Reservations> findByTimestampPaid(Timestamp forSearchOnly);
    
}
