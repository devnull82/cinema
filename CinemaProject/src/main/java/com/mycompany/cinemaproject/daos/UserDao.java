/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.daos;

import com.mycompany.cinemaproject.entities.Users;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Krisz
 */
public interface UserDao extends JpaRepository<Users, Long>{
    Users findByEmail(String name);
    
}
