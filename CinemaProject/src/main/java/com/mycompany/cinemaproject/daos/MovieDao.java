/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.daos;

import com.mycompany.cinemaproject.entities.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author OKTATAS
 */
public interface MovieDao extends JpaRepository<Movies, Long>{
    
}
