package com.mycompany.cinemaproject.daos;

import com.mycompany.cinemaproject.entities.Shows;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author OKTATAS
 */
public interface ShowDao extends JpaRepository<Shows, Long>{
    
}
