/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.controllers;

import com.mycompany.cinemaproject.datatypes.ShowDTO;
import com.mycompany.cinemaproject.services.ShowService;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SKCADMIN
 */
@RestController
@RequestMapping(path = "/shows")
public class ShowsController {

    @Autowired
    ShowService showservice;

    @RequestMapping(method = RequestMethod.GET)
    public List<ShowDTO> showShows(@RequestParam(name = "cinemaid", defaultValue = "-1") Long cinemaId,
            @RequestParam(name = "day", defaultValue = "19700101") @DateTimeFormat(pattern = "yyyyMMdd") Date day,
            @RequestParam(name = "movieid", defaultValue = "-1") Long movieId) {
        return showservice.getShows(cinemaId, new Timestamp(day.getTime()), movieId);
    }

    @RequestMapping(path = {"/{id}"}, method = RequestMethod.GET)
    public ShowDTO showOneShow(@PathVariable("id") Long id) {
        return showservice.getShow(id);

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/{id}"}, method = RequestMethod.DELETE)
    public Long deleteShow(@PathVariable("id") Long id) {
        return showservice.deleteShow(id);

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(method = RequestMethod.POST)
    public ShowDTO addShow(ShowDTO showDTO) {
        return showservice.addShow(showDTO);
    }
}
