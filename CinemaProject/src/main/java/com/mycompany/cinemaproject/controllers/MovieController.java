/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.controllers;

import com.mycompany.cinemaproject.datatypes.CinemaAndShowDTO;
import com.mycompany.cinemaproject.datatypes.MovieDTO;
import com.mycompany.cinemaproject.datatypes.Rating;
import com.mycompany.cinemaproject.services.MovieService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author OKTATAS
 */
@RestController
@RequestMapping(path = "/movies")
public class MovieController {
    
    @Autowired
    MovieService ms;
    
    public static final Logger LOG = LoggerFactory.getLogger(MovieController.class);
    
    @GetMapping()
    public List<MovieDTO> listOfAllMovies(){
        return ms.getAllMovies();
    }
    
    @GetMapping(path = "/{id}")
    public MovieDTO singleMovie(@PathVariable Long id){
        LOG.debug("singleMovie started");
        return ms.getMovie(id);
    }
    @Secured("ROLE_ADMIN")
    @DeleteMapping(path = "/{id}")
    public Long deleteSingleMovie(@PathVariable Long id){
        return ms.deleteMovie(id);
    }
    
    @GetMapping(path="/{id}/reservation")
    public List<CinemaAndShowDTO> reservationHelperGetCinemasAndShows(@PathVariable Long id){
        List<CinemaAndShowDTO> cinemaandshowList = ms.reservationHelperGetCinemasAndShows(id);
        return cinemaandshowList;
    }
    @Secured("ROLE_ADMIN")
    @PostMapping()
    public Long addMovie(@RequestBody MovieDTO movie){
        return ms.addMovie(movie);
    }
    @Secured("ROLE_ADMIN")
    @GetMapping(path="/tmdb")
    public MovieDTO getTMDBMovie(@RequestParam(name = "id") Long id){
        return ms.getTMDBMovie(id);
    }
    @Secured("ROLE_ADMIN")
    @PutMapping(path = "/{id}")
    public MovieDTO modifyMovie(@PathVariable Long id, @RequestBody MovieDTO newData){
        return ms.modifyMovie(newData);
    }
    
    @Secured("ROLE_ADMIN")
    @GetMapping(path = "/ratings")
    public Rating[] returnAllRatings(){
        Rating[] ratings = {Rating.A12, Rating.A16, Rating.A18};
        return ratings;
    }
    
}
