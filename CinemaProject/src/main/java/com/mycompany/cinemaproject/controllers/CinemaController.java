/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.controllers;

import com.mycompany.cinemaproject.datatypes.CinemaDTO;
import com.mycompany.cinemaproject.datatypes.MovieDTO;
import com.mycompany.cinemaproject.datatypes.RoomsDTO;
import com.mycompany.cinemaproject.datatypes.RowsDTO;
import com.mycompany.cinemaproject.services.CinemaService;
import com.mycompany.cinemaproject.services.MovieService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SKCADMIN
 */
@RestController
@RequestMapping(path = {"/cinemas"})
public class CinemaController {

    @Autowired
    CinemaService cinemaService;

    @Autowired
    MovieService movieservice;

    @RequestMapping(method = RequestMethod.GET)
    public List<CinemaDTO> showCinemas() {
        System.out.println("showCinemas called");
        List<CinemaDTO> cinemas = cinemaService.returnCinemas();

        return cinemas;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(method = RequestMethod.POST)//PUT
    public CinemaDTO addNewCinema(@RequestBody CinemaDTO cDTO) {

        return cinemaService.addNewCinema(cDTO);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/{id}"}, method = RequestMethod.DELETE)
    public Long deleteCinema(@PathVariable("id") Long id) {

        boolean ok = cinemaService.deleteCinema(id);

        return id;

    }

    @RequestMapping(path = {"/{id}"}, method = RequestMethod.GET)
    public CinemaDTO showOneCinema(@PathVariable("id") Long id) {

        CinemaDTO cDTO = cinemaService.getOneCinema(id);

        return cDTO;

    }

    @RequestMapping(path = {"/{id}/movies"}, method = RequestMethod.GET)
    public List<MovieDTO> showCinemaMovies(@PathVariable("id") Long cinemaId) {
        return movieservice.getMoviesWhereCinemaShowsThem(cinemaId);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/{id}"}, method = RequestMethod.PUT)//POST
    public CinemaDTO modifyCinemas(@PathVariable("id") Long id, @RequestBody CinemaDTO cDTO) {

        cDTO = cinemaService.modifyCinemas(id, cDTO);

        return cDTO;

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/addRoom/{id}"}, method = RequestMethod.POST)
    public RoomsDTO addNewRoomToCinema(@PathVariable("id") Long cinemaid, @RequestBody String name) {

        RoomsDTO roomsDTO = cinemaService.addNewRoom(cinemaid, name);

        return roomsDTO;

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/modifyRoom/{id}"}, method = RequestMethod.PUT)
    public RoomsDTO modifyRoom(@PathVariable("id") Long roomid, @RequestBody String name) {

        RoomsDTO roomsDTO = cinemaService.modifyRoom(roomid, name);

        return roomsDTO;

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/modifyRoom/{id}"}, method = RequestMethod.DELETE)
    public RoomsDTO deleteRoom(@PathVariable("id") Long roomid, @RequestBody String name) {

        RoomsDTO roomsDTO = cinemaService.deleteRoom(roomid);

        return roomsDTO;

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/addRow/{id}"}, method = RequestMethod.POST)
    public RowsDTO[] addNewRowToRoom(@PathVariable("id") Long roomID, @RequestBody RowsDTO[] rowsDTOs) {
        
        for (int i = 0; i < rowsDTOs.length; i++) {
            rowsDTOs[i] = cinemaService.addNewRowToRoom(roomID, rowsDTOs[i]);
        }
        
        cinemaService.fixRowNumbersOfRoom(roomID);
        
        return rowsDTOs;

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"/addRow/{id}"}, method = RequestMethod.DELETE)
    public RowsDTO deleteRow(@PathVariable("id") Long rowsID) {
        RowsDTO rowsDTO = new RowsDTO();
        rowsDTO = cinemaService.deleteRow(rowsID);

        return rowsDTO;

    }

    @RequestMapping(path = {"rooms/{id}/rows"}, method = RequestMethod.GET)
    public List<RowsDTO> showRows(@PathVariable("id") Long roomID) {
        List<RowsDTO> rowsList = cinemaService.showAllRows(roomID);

        return rowsList;

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = {"rooms/rows/{rowID}"}, method = RequestMethod.PUT)
    public RowsDTO modifyRow(@PathVariable("rowID") Long rowID, @RequestBody RowsDTO rowDTO) {
        RowsDTO rows = cinemaService.modifyRow(rowID, rowDTO);
        return rows;

    }

    @RequestMapping(path = {"/showRooms/{id}"}, method = RequestMethod.GET)
    public List<RoomsDTO> showCinemasRooms(@PathVariable("id") Long id) {
        RoomsDTO roomDTO = new RoomsDTO();
        List<RoomsDTO> roomsList = cinemaService.showRooms(id);

        return roomsList;

    }

    @RequestMapping(path = {"/showRooms"}, method = RequestMethod.GET)
    public List<RoomsDTO> showCinemasAllRooms() {
        RoomsDTO roomDTO = new RoomsDTO();
        List<RoomsDTO> roomsList = cinemaService.showAllRooms();
        return roomsList;

    }

    @RequestMapping(path = {"/showOneRoom/{id}"}, method = RequestMethod.GET)
    public RoomsDTO showOneRoom(@PathVariable("id") Long roomID) {
        RoomsDTO roomDTO = cinemaService.showOneRoom(roomID);
        return roomDTO;
    }

}
