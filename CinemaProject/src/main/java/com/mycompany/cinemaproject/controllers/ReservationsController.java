package com.mycompany.cinemaproject.controllers;

import com.mycompany.cinemaproject.datatypes.ReservationDTO;
import com.mycompany.cinemaproject.datatypes.RowAndSeat;
import com.mycompany.cinemaproject.exceptions.InvalidReservationException;
import com.mycompany.cinemaproject.services.ReservationService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Krisz
 */
@RestController
@RequestMapping(path = {"/reservations"})
public class ReservationsController {

    @Autowired
    ReservationService reservationService;
    
    @ExceptionHandler(InvalidReservationException.class)
    public ResponseEntity<String> handleRestErrors(InvalidReservationException ire) {
        ResponseEntity<String> ret = new ResponseEntity<>(ire.getMessage(), HttpStatus.I_AM_A_TEAPOT);
        return ret;
    }

    @RequestMapping(method = RequestMethod.POST)
    public List<Long> addNewReservation (@RequestBody ReservationDTO rDTO) throws InvalidReservationException {
        return reservationService.addNewReservation(rDTO);
    }
    
    @RequestMapping(path = "/{id}",method = RequestMethod.GET)
    public List<RowAndSeat> getReservedSeatsInASpecificShow (@PathVariable("id") Long id) {
        
        return reservationService.reservedSeats(id);
        
    }
    
    //TODO reservationForUserByEmail
    //TODO getUserByEmailAndActiveReservations
    
    @GetMapping(path = "/ofuser/{id}")
    public List<ReservationDTO> getReservationsOfUser(@PathVariable(name = "id") Long userId){
        return reservationService.reservationsOfUser(userId);
    }
        @GetMapping(path = "/ofuserAll/{id}")
    public List<ReservationDTO> allReservationOfTheUsers(@PathVariable(name = "id") Long userId){
        return reservationService.allresevations(userId);
    }
    
}
