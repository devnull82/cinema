/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.controllers;

import com.mycompany.cinemaproject.datatypes.UserDTO;
import com.mycompany.cinemaproject.exceptions.VerifyException;
import com.mycompany.cinemaproject.services.VerifyService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SKCADMIN
 */
@RestController
@RequestMapping(path = {"/verify"})
public class VerifyController {

    @Autowired
    VerifyService vf;

    //@Autowired    
    // public UserDetailsManager userDetailsManager;
    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @RequestMapping(method = RequestMethod.GET, path = "/{id}/{hash}")
    public UserDTO verify(
            @PathVariable("id") Long id,
            @PathVariable("hash") String hash, HttpServletRequest request) throws VerifyException, VerifyException {
        UserDTO udto = vf.verify(id, hash);

        String username = udto.getEmail();

        authenticateUserAndInitializeSessionByUsername(username, userDetailsService, request);

        return udto;
    }

    public boolean authenticateUserAndInitializeSessionByUsername(String username, UserDetailsService userDetailsService, HttpServletRequest request) {
        boolean result = true;

        try {
            // generate session if one doesn't exist
            request.getSession();

            // Authenticate the user
            UserDetails user = userDetailsService.loadUserByUsername(username);
            //UserDetails user = userDetailsManager.loadUserByUsername(username);
            Authentication auth = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);
        } catch (Exception e) {
            System.out.println(e.getMessage());

            result = false;
        }

        return result;
    }

}
