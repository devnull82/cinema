package com.mycompany.cinemaproject.controllers;

import com.mycompany.cinemaproject.daos.UserDao;
import com.mycompany.cinemaproject.datatypes.UserDTO;
import com.mycompany.cinemaproject.entities.Users;
import com.mycompany.cinemaproject.exceptions.UserAlreadyRegisteredException;
import com.mycompany.cinemaproject.exceptions.VerifyException;
import com.mycompany.cinemaproject.services.UserService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import static java.util.Objects.isNull;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Krisz
 */
@RestController
@RequestMapping(path = {"/users"})
public class UserController {
    
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Autowired
    UserDao uDao;

    @ExceptionHandler(UserAlreadyRegisteredException.class)
    public ResponseEntity<String> handleRestErrors(UserAlreadyRegisteredException ex) {
        ResponseEntity<String> ret = new ResponseEntity<>(ex.getMessage(), HttpStatus.I_AM_A_TEAPOT);
        return ret;
    }

    @RequestMapping(method = RequestMethod.POST)
    public UserDTO registerUser(@RequestBody UserDTO uDTO) throws NoSuchAlgorithmException, UnsupportedEncodingException, UserAlreadyRegisteredException {

        if (userService.findUser(uDTO) != null) {
            throw new UserAlreadyRegisteredException("Email address is already in use.");
        } else {
            return userService.addNewUser(uDTO);
        }
    }

    @RequestMapping(path = {"/current"}, method = RequestMethod.GET)
    public UserDTO current(HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        UserDTO udto = new UserDTO();
        try {
            Users u = userService.findUserWithAuthorities(name);
            udto = new UserDTO(u);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }

        return udto;
    }

    @RequestMapping(path = {"/settings/modifyPassword/{id}/{verifyHash}"}, method = RequestMethod.PUT)
    public UserDTO modify(@RequestBody UserDTO udto, @PathVariable("verifyHash") String hash, @PathVariable("id") Long id) throws VerifyException {

        udto.setId(id);

        return userService.modifyPassword(udto, hash);

    }

    @RequestMapping(path = {"/settings/modifyPassword/{id}/{verifyHash}"}, method = RequestMethod.GET)
    public UserDTO modifyGet(@PathVariable("verifyHash") String hash, @PathVariable("id") Long id) throws VerifyException {

        UserDTO udto = new UserDTO(uDao.findOne(id));
        udto = new UserDTO(userService.findUserWithAuthorities(udto.getEmail()));
        return udto;
    }

    @RequestMapping(path = {"/settings/modifyPassword"}, method = RequestMethod.POST)
    public UserDTO modify(@RequestBody UserDTO udto) throws VerifyException {

        if (isNull(udto.getFull_name())) {
            return userService.sendModifyPasswordMail(udto);
        } else {
            return userService.setNewPassWord(udto);
        }

    }

    //SetAvatar
    @RequestMapping(path = {"/settings/{id}/setAvatar"}, method = RequestMethod.POST)
    public UserDTO setAvatar(@PathVariable("id") Long id, @RequestParam("file") MultipartFile file) throws IOException, VerifyException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users u = uDao.findOne(id);
        LOG.info("setAvatar u.getEmail() is {} and name is: {}", u.getEmail(), name);
        if (u.getEmail().equals(name)) {
            return userService.setAvatar(id, file);
        } else {
            throw new VerifyException("Not the same user");
        }

    }

    @RequestMapping(value = "/{id}/getPicture", method = RequestMethod.GET)
    public byte[] showImage(HttpServletResponse response, @PathVariable("id") Long id) throws Exception {
        return uDao.findOne(id).getFile();
    }

    @GetMapping("/byemail")
    public UserDTO getUserByemail(@RequestParam(name = "email") String email) {
        return userService.findUserByEmail(email);
    }

    @RequestMapping(path = {"/settings/modifyName/{id}/{hash}"}, method = RequestMethod.POST)
    public UserDTO modifyName(@PathVariable("id") Long id, @PathVariable("hash") String hash, @RequestBody UserDTO udto) throws IOException, VerifyException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        udto.setId(id);
        Users u = uDao.findOne(id);
        if (u.getEmail().equals(name)) {

            return userService.modifyName(udto, hash);
        } else {
            throw new VerifyException("Not the same user");
        }

    }

}
