/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.mailSending;

/**
 *
 * @author SKCADMIN
 */
import com.mycompany.cinemaproject.datatypes.Mail;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;
import org.springframework.core.io.FileSystemResource;

@Service("mailService")
public class MailServiceImpl implements MailService {

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    Configuration fmConfiguration;

    @Override
    public void sendEmail(Mail mail) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        try {

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(mail.getMailFrom());
            mimeMessageHelper.setTo(mail.getMailTo());
            mail.setMailContent(geContentFromTemplate(mail.getModel(), mail.getTemplate()));
            mimeMessageHelper.setText(mail.getMailContent(), true);

            //Attachment hozzáadása, TODO nem teljes elv működik!
            /*
            if (!mail.getAttachments().isEmpty()) {
                for (Object attachment : mail.getAttachments()) {
                    FileSystemResource file = new FileSystemResource((String) attachment);
                    mimeMessageHelper.addAttachment(file.getFilename(), file);
                }

            }
             */
            mailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public String geContentFromTemplate(Map< String, Object> model, Integer i) {
        StringBuffer content = new StringBuffer();

        String template = "";

        try {
            switch (i) {
                case 1:
                    template = "password.html";
                    break;
                case 2:
                    template = "order.html";
                    break;
                case 3:
                    template = "forgotpassword.html";
                    break;
                case 4:
                    template = "modifypassword.html";
                    break;
            }
            content.append(FreeMarkerTemplateUtils
                    .processTemplateIntoString(fmConfiguration.getTemplate(template), model));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }

}
