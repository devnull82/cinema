/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.security;

import com.mycompany.cinemaproject.daos.UserDao;
import com.mycompany.cinemaproject.entities.Users;
import com.mycompany.cinemaproject.services.UserService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 *
 * @author Krisz
 */
public class UserDetailsServiceImpl implements UserDetailsService{
    
    Logger LOG = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    
    @Autowired
    UserDao uDao;
    
    @Autowired
    UserService uService;
    

    @Override
    public Users loadUserByUsername(String username) throws UsernameNotFoundException {
        LOG.debug("We will load user: {}", username);
        Users userByEmail = uService.findUserWithAuthorities(username);
        if(userByEmail==null) throw new UsernameNotFoundException("User " + username + " not found.");
        else return userByEmail;
    }
    
}
