package com.mycompany.cinemaproject.security;

import com.mycompany.cinemaproject.config.MyServletConfig;
import com.mycompany.cinemaproject.filter.UTF8Filter;
import java.util.Arrays;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired
    private DataSource dataSource;
    
    @Autowired
    UserDetailsService userDetailsService;
    
    @Autowired
    CustomAuthenticationSuccessHandler successHandler;
    
    @Autowired
    CustomAuthenticationFailureHandler failureHandler;
    
    @Autowired
    CustomLogoutSuccessHandler logouthandler;
    
     @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList(MyServletConfig.ALLOWED_CORS_ORIGINS));
        configuration.setAllowedMethods(Arrays.asList("*"));
        configuration.setAllowedHeaders(Arrays.asList(MyServletConfig.ALLOWED_HEADERS));
        configuration.setAllowCredentials(Boolean.TRUE);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration(MyServletConfig.CORS_MAPPNIG, configuration);
        return source;
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetailsService manager = new UserDetailsServiceImpl();
        return manager;
    }
    
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    
    
    /*@Bean
    public CustomAuthenticationProvider authProvider(){
        CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider();
        return authProvider;
    
    }*/
    
    @Bean
    public DaoAuthenticationProvider authProvider(){
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.authenticationProvider(authProvider());
    }
        
     
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        UTF8Filter filter = new UTF8Filter();
        http//.addFilterBefore(filter,CsrfFilter.class)
                .csrf().disable()                
                //.authenticationProvider(authProvider())
                .formLogin()
                .usernameParameter("email")//.loginProcessingUrl("/cinemalogin")                
                //.formLogin().loginPage("/temp/login")
                //.loginProcessingUrl("/cinemalogin")                
                //.loginProcessingUrl("/temp/login")
                //.defaultSuccessUrl("/listmessages")
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .and().logout().logoutSuccessHandler(logouthandler)
                .and().cors()
                .and()
                .authorizeRequests()
                //.antMatchers("/temp/login").permitAll()
                //.antMatchers("/rest/**").permitAll()
                //.antMatchers("/temp/register").permitAll()
                //.anyRequest().authenticated()
                .antMatchers("/cinemas/addRow/**").authenticated()
                .anyRequest().permitAll();
                
                //.and()
                //.logout()
                //.logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
    }
    
       @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
   @Override
   public AuthenticationManager authenticationManagerBean() throws Exception {
       return super.authenticationManagerBean();
   }
    

}