package com.mycompany.cinemaproject.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author Krisz
 */
@Component
public class PasswordEncoderGenerator {
    
    public PasswordEncoder getEncoder(){
        return new BCryptPasswordEncoder();
    }

}
