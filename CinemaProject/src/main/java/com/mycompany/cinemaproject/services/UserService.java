package com.mycompany.cinemaproject.services;

import com.mycompany.cinemaproject.daos.UserDao;
import com.mycompany.cinemaproject.datatypes.Mail;
import com.mycompany.cinemaproject.datatypes.RegistrationModes;
import com.mycompany.cinemaproject.datatypes.UserDTO;
import com.mycompany.cinemaproject.entities.Authorities;
import com.mycompany.cinemaproject.entities.Users;
import com.mycompany.cinemaproject.exceptions.VerifyException;
import com.mycompany.cinemaproject.mailSending.MailService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Krisz
 */
@Component
public class UserService {

    @PersistenceContext
    EntityManager em;

    @Autowired
    UserDao uDao;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MailService mailservice;

    @Transactional
    public UserDTO addNewUser(UserDTO userDto) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Users userToDb = new Users(userDto);
        userToDb.setAuthorities(new ArrayList<Authorities>());
        userToDb.setPassword(passwordEncoder.encode(userToDb.getPassword()));
        userToDb.setVerify_hash(generateVerifyHash());
        userToDb.setRegistration_mode(RegistrationModes.STANDARD);
        userToDb.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        em.persist(userToDb);

        Authorities authorityToDb = new Authorities(userToDb, "ROLE_USER");
        userToDb.addAuthorities(authorityToDb);
        em.persist(authorityToDb);

        em.flush();

        userDto = new UserDTO(userToDb);

        //Testing the mail sending   
        Mail mail = new Mail();
        mail.setMailFrom("progmaticprojectcinema@gmail.com");
        mail.setMailTo(userToDb.getEmail());
        mail.setMailSubject("Registration");

        String url = "http://cinema.project.progmatic.hu/verify/"
                + userDto.getId()
                + "/"
                + userToDb.getVerify_hash();

        Map< String, Object> model = new HashMap<>();
        model.put("firstName", userToDb.getFull_name());
        model.put("url", url);
        mail.setModel(model);
        mail.setTemplate(1); //Template 1 = register template, template = 2  order

        mailservice.sendEmail(mail);
        return userDto;
    }

    public String generateVerifyHash() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String input = (String.valueOf(Timestamp.valueOf(LocalDateTime.now())) + "AddMoreTextHere");
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(input.getBytes("UTF-8"));
        byte[] byteArray = digest.digest();
        String ret = "";
        for (int i = 0; i < byteArray.length; i++) {
            ret = ret + byteArray[i];
        }
        return ret;

    }

    public Users findUserWithAuthorities(String email) {
        EntityGraph eg = em.getEntityGraph("userWithAuthorities");
        Users ret = (Users) em.createQuery("select user from Users user WHERE user.email = :email")
                .setParameter("email", email)
                .setHint("javax.persistence.loadgraph", eg)
                .getSingleResult();
        return ret;
    }

    public Users findUser(UserDTO uDto) {
        return uDao.findByEmail(uDto.getEmail());
    }

    public UserDTO modifyPassword(UserDTO udto, String hash) throws VerifyException {

        Users u = uDao.findOne(udto.getId());
        u = findUserWithAuthorities(u.getEmail());
        if (u.getVerify_hash().equals(hash)) {

            u.setPassword(passwordEncoder.encode(udto.getPassword()));
            uDao.save(u);
            udto = new UserDTO(u);

            return udto;

        } else {

            throw new VerifyException("HashCode mismatch");
        }

    }

    public UserDTO sendModifyPasswordMail(UserDTO udto) {

        Users u = findUserWithAuthorities(udto.getEmail());

        Mail mail = new Mail();
        mail.setMailFrom("progmaticprojectcinema@gmail.com");
        mail.setMailTo(u.getEmail());
        mail.setMailSubject("ModifyPass");

        String url = "http://cinema.project.progmatic.hu/users/settings/modifyPassword/"
                + u.getId()
                + "/"
                + u.getVerify_hash();

        Map< String, Object> model = new HashMap<>();
        model.put("url", url);
        mail.setModel(model);
        mail.setTemplate(3); //Template 1 = register template, template = 2  order, 3 = fogotpassword;

        mailservice.sendEmail(mail);

        return udto = new UserDTO(u);

    }

    public UserDTO setAvatar(Long id, MultipartFile file) throws IOException {

        byte[] picture = file.getBytes();

        Users u = uDao.findOne(id);
        u.setFile(picture);
        uDao.save(u);
        return new UserDTO(findUserWithAuthorities(u.getEmail()));
    }

    public UserDTO findUserByEmail(String email) {
        EntityGraph eg = em.getEntityGraph("userWithAuthorities");
        try {
            return new UserDTO(em.createQuery("select u from Users u where u.email=:email", Users.class)
                    .setParameter("email", email)
                    .setHint("javax.persistence.loadgraph", eg)
                    .getSingleResult());
        } catch (Exception e) {
            throw new RuntimeException("No user has this e-mail adress in the database");
        }
    }

    public UserDTO setNewPassWord(UserDTO udto) {

        Users u = findUserWithAuthorities(udto.getEmail());

        Mail mail = new Mail();
        mail.setMailFrom("progmaticprojectcinema@gmail.com");
        mail.setMailTo(u.getEmail());
        mail.setMailSubject("ModifyPass");

        String url = "http://cinema.project.progmatic.hu/users/settings/modifyPassword/"
                + u.getId()
                + "/"
                + u.getVerify_hash();

        Map< String, Object> model = new HashMap<>();
        model.put("url", url);
        mail.setModel(model);
        mail.setTemplate(4); //Template 1 = register template, template = 2  order, 3 = forgotpassword,4 = MODIFYPASS;

        mailservice.sendEmail(mail);

        return udto = new UserDTO(u);
    }

    public UserDTO modifyName(UserDTO udto, String hash) throws VerifyException {

        Users u = uDao.findOne(udto.getId());
        u = findUserWithAuthorities(u.getEmail());

        if (u.getVerify_hash().equals(hash)) {

            u.setFull_name(udto.getFull_name());
            uDao.save(u);
            return findUserByEmail(u.getEmail());
        } else {
            throw new VerifyException("HashCode Mismatch");
        }

    }

}
