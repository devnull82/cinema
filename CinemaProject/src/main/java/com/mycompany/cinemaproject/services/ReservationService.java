/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.itextpdf.barcodes.Barcode128;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.mycompany.cinemaproject.daos.ReservationDao;
import com.mycompany.cinemaproject.daos.RowsDao;
import com.mycompany.cinemaproject.daos.ShowDao;
import com.mycompany.cinemaproject.daos.UserDao;
import com.mycompany.cinemaproject.datatypes.Mail;
import java.io.FileNotFoundException;
import com.mycompany.cinemaproject.datatypes.ReservationDTO;
import com.mycompany.cinemaproject.datatypes.RowAndSeat;
import com.mycompany.cinemaproject.entities.Reservations;
import com.mycompany.cinemaproject.entities.Rows;
import com.mycompany.cinemaproject.entities.Shows;
import com.mycompany.cinemaproject.entities.Users;
import com.mycompany.cinemaproject.exceptions.InvalidReservationException;
import com.mycompany.cinemaproject.mailSending.MailService;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author OKTATAS
 */
@Component
public class ReservationService {

    @Autowired
    ReservationDao rd;

    @Autowired
    ShowDao sd;

    @Autowired
    UserDao ud;

    @Autowired
    RowsDao rowD;

    @PersistenceContext
    EntityManager em;

    @Autowired
    ReservationService me;

    @Autowired
    MailService mailservice;

    //prototype-but-works
    @Transactional
    public void createETicketPDF(List<Reservations> reservations) throws FileNotFoundException, IOException {
        for (Reservations reservation : reservations) {
            Reservations managedReservetion = em.find(Reservations.class, reservation.getId());
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            PdfWriter writer = new PdfWriter(os);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);
            document.add(new Paragraph(String.format("%s\n%s\n%s", reservation.getShow().getRoom().getCinema().getName(), reservation.getShow().getRoom().getRoomName(),
                    reservation.getShow().getMovie().getTitle())));
            float[] columnWidths = {0f};
            Table table = new Table(columnWidths);
            table.addCell(create128barcodeCell(String.format("%1$d%1$d%1$d%1$d%1$d%1$d%1$d%1$d", 1), pdf));
            document.add(table);
            document.flush();
            writer.flush();
            managedReservetion.setEticket(os.toByteArray());
            document.close();
        }
    }

    private Cell create128barcodeCell(String code, PdfDocument pdf) {
        Barcode128 code128 = new Barcode128(pdf);
        code128.setCodeType(Barcode128.CODE128);
        code128.setCode(code);
        Cell cell = new Cell().add(new Image(code128.createFormXObject(Color.BLACK, Color.BLACK, pdf)));
        cell.setBorder(Border.NO_BORDER);
        cell.setPaddingBottom(10);
        return cell;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public List<Long> addNewReservation(ReservationDTO rDTO) throws InvalidReservationException {
        List<Long> reservationIdList = new ArrayList<>();
        boolean validReservationRequest = me.validateReservationRequest(rDTO);
        if (validReservationRequest) {
            List<Reservations> reservationList = buildReservationList(rDTO);
            for (Reservations reservation : reservationList) {
                em.persist(reservation);

            }
            em.flush();
            try {
                me.createETicketPDF(reservationList);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            //TODO
            Reservations reservation = rd.findOne(reservationList.get(0).getId());
            String cinemaAdress = reservation.getShow().getRoom().getCinema().getAddress();
            String cinemaName = reservation.getShow().getRoom().getCinema().getName();

            Users u = ud.findOne(rDTO.getUserId());
            Mail mail = new Mail();
            mail.setMailFrom("progmaticprojectcinema@gmail.com");
            mail.setMailTo(u.getEmail());
            mail.setMailSubject("Reserved Tickets");

            Map< String, Object> model = new HashMap<>();
            model.put("PaymentMethod", rDTO.getPayment_mode());
            model.put("Name", u.getFull_name());

            model.put("EmailAdress", u.getEmail());
            model.put("cinemaName", cinemaName);
            model.put("cinemaAdress", cinemaAdress);
            model.put("OrderID", reservation.getId());
            model.put("movieTitle", rDTO.getTitle());
            List<RowAndSeat> rowAndSeatList = rDTO.getRowAndSeatList();
            model.put("rowAndSeatList", rowAndSeatList);
            Long total = rowAndSeatList.size() * rDTO.getPrice();
            model.put("total", total);
            model.put("price", rDTO.getPrice());

            mail.setModel(model);
            mail.setTemplate(2);
            mailservice.sendEmail(mail);

            //reservationList = getReservationList(rDTO);
            reservationIdList = buildReservationIdList(reservationList);
        }
        if (reservationIdList.isEmpty()) {
            throw new InvalidReservationException("Seat(s) has already been reserved/invalid reservation request.");
        } else {
            return reservationIdList;
        }
    }

    @Transactional
    public List<RowAndSeat> reservedSeats(Long showid) {
        EntityGraph eg = em.getEntityGraph("reservationGraph");
        List<Reservations> reservationList = (List<Reservations>) em.createQuery("Select r from Reservations r join r.show s join r.row z where s.id= :showID")
                .setParameter("showID", showid).setHint("javax.persistence.loadgraph", eg).getResultList();
        List<RowAndSeat> rasList = new ArrayList<>();
        for (Reservations reservations : reservationList) {
            RowAndSeat r = new RowAndSeat(reservations.getRow().getRowNumber(), reservations.getSeat(), reservations.getRow().getId());
            rasList.add(r);
        }
        return rasList;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public boolean validateReservationRequest(ReservationDTO rDTO) {
        boolean validReservationRequest = true;
        //ezekben a sorokban van foglalás
        Set<Long> rowsInReservation = new HashSet<>();
        for (RowAndSeat ras : rDTO.getRowAndSeatList()) {
            rowsInReservation.add(ras.getRowId());
        }
        //soronként ellenőrizzük a foglalásokat
        for (Long rowId : rowsInReservation) {
            //a sorban levő székeket egy int[] reprezentálja, 0 - szabad, 1 - foglalt, 2 - ütköző foglalás
            int[] seats = new int[rowD.getOne(rowId).getSeats()];
            //megjelöljük a már lefoglalt helyeket
            List<RowAndSeat> alreadyReserved = reservedSeats(rDTO.getShowId());
            for (RowAndSeat ras : alreadyReserved) {
                if (ras.getRowId().equals(rowId)) {
                    seats[ras.getSeat() - 1]++;
                }
            }
            //megjelöljük azokat a helyeket, amelyekre a foglalási igényt megkaptuk
            List<RowAndSeat> reservationRequest = rDTO.getRowAndSeatList();
            for (RowAndSeat ras : reservationRequest) {
                if (ras.getRowId().equals(rowId)) {
                    //olyan székre érkezett foglalási igény, ami nem létezik
                    if (ras.getSeat() > seats.length) {
                        return false;
                    }
                    seats[ras.getSeat() - 1]++;
                }
            }
            //ütköző foglalások keresése (2-es érték)
            for (int i = 0; i < seats.length; i++) {
                if (seats[i] > 1) {
                    validReservationRequest = false;
                    break;
                }
            }
            //kimaradó helyek keresése
            if ((seats[0] == 0 && seats[1] == 1) || (seats[seats.length - 2] == 1 && seats[seats.length - 1] == 0)) {
                validReservationRequest = false;
                break;
            }
            for (int i = 1; i < seats.length - 1; i++) {
                if (seats[i] == 0 && seats[i - 1] == 1 && seats[i + 1] == 1) {
                    validReservationRequest = false;
                    break;
                }
            }
        }
        return validReservationRequest;
    }

    private List<Reservations> buildReservationList(ReservationDTO rDTO) {
        List<Reservations> reservationList = new ArrayList<>();
        for (RowAndSeat ras : rDTO.getRowAndSeatList()) {
            Reservations r = new Reservations();
            r.setSeat(ras.getSeat());
            r.setRow(rowD.getOne(ras.getRowId()));
            r.setTimestampReserved(Timestamp.valueOf(LocalDateTime.now()));
            r.setTimestamp_paid(Timestamp.valueOf(LocalDateTime.now()));
            r.setPayment_mode(rDTO.getPayment_mode());
            r.setShow(sd.findOne(rDTO.getShowId()));
            r.setUser(ud.findOne(rDTO.getUserId()));
            reservationList.add(r);
        }
        return reservationList;
    }

    private List<Reservations> getReservationList(ReservationDTO rDTO) {
        List<Reservations> ret = new ArrayList<>();
        for (RowAndSeat ras : rDTO.getRowAndSeatList()) {
            Reservations reservation = (Reservations) em.createQuery("Select r from Reservations r where r.show=:show and r.row=:row and r.seat=:seat")
                    .setParameter("show", sd.findOne(rDTO.getShowId()))
                    .setParameter("row", rowD.findOne(ras.getRowId()))
                    .setParameter("seat", ras.getSeat())
                    .getSingleResult();
            ret.add(reservation);
        }
        return ret;
    }

    private List<Long> buildReservationIdList(List<Reservations> reservationList) {
        List<Long> ret = new ArrayList<>();
        for (Reservations reservations : reservationList) {
            ret.add(reservations.getId());
        }
        return ret;
    }

    public List<ReservationDTO> reservationsOfUser(Long userId) {
        List<ReservationDTO> resDTOlist = new LinkedList<>();
        List<Long> showIdList = em.createQuery("select distinct s.id from Reservations r join r.show s join r.user u where u.id=:userId and s.timestamp>:currentTime")
                .setParameter("userId", userId)
                .setParameter("currentTime", new Timestamp(new Date().getTime()))
                .getResultList();
        for (Long showId : showIdList) {
            List<Reservations> reservationList = em.createQuery("select r from Reservations r join r.show s join r.user u where u.id=:userId and s.id=:showId")
                    .setParameter("userId", userId)
                    .setParameter("showId", showId)
                    .getResultList();
            List<RowAndSeat> rowAndSeatList = new ArrayList<>();
            for (Reservations reservation : reservationList) {
                rowAndSeatList.add(new RowAndSeat(reservation.getRow().getRowNumber(), reservation.getSeat(), reservation.getRow().getId()));
            }
            resDTOlist.add(new ReservationDTO(reservationList.get(0), rowAndSeatList));
        }
        return resDTOlist;
    }

    public List<ReservationDTO> allresevations(Long userId) {
        List<ReservationDTO> resDTOlist = new LinkedList<>();
        List<Long> showIdList = em.createQuery("select distinct s.id from Reservations r join r.show s join r.user u where u.id=:userId ")
                .setParameter("userId", userId)
                .getResultList();
        for (Long showId : showIdList) {
            List<Reservations> reservationList = em.createQuery("select r from Reservations r join r.show s join r.user u where u.id=:userId and s.id=:showId")
                    .setParameter("userId", userId)
                    .setParameter("showId", showId)
                    .getResultList();
            List<RowAndSeat> rowAndSeatList = new ArrayList<>();
            for (Reservations reservation : reservationList) {
                rowAndSeatList.add(new RowAndSeat(reservation.getRow().getRowNumber(), reservation.getSeat(), reservation.getRow().getId()));
            }
            resDTOlist.add(new ReservationDTO(reservationList.get(0), rowAndSeatList));
        }
        return resDTOlist;
    }

}
