/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.mycompany.cinemaproject.daos.UserDao;
import com.mycompany.cinemaproject.datatypes.UserDTO;
import com.mycompany.cinemaproject.entities.Users;
import com.mycompany.cinemaproject.exceptions.VerifyException;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author SKCADMIN
 */
@Component
public class VerifyService {

    @Autowired
    UserDao uDao;
    
    @PersistenceContext
    EntityManager em;

    @Transactional
    public UserDTO verify(Long id, String hash) throws VerifyException {

        Users u = uDao.findOne(id);
        UserDTO userDTO = new UserDTO();
        if (!u.isIs_verified()) {
            if (u.getVerify_hash().equals(hash)) {
                EntityGraph eg = em.getEntityGraph("userWithAuthorities");
                u.setIs_verified(true);
                u  =  (Users) em.createQuery("Select u from Users u where u.id=:userID").setHint("javax.persistence.loadgraph",eg).setParameter("userID", id).getResultList().get(0);
                userDTO = new UserDTO(u);

            } else {
                throw new VerifyException("Hash Code mismatch");
            }
        } else {
            throw new VerifyException("Already Verified");
        }
        return userDTO;

    }

}
