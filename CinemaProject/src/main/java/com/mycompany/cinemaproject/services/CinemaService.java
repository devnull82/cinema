/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.mycompany.cinemaproject.daos.RowsDao;
import com.mycompany.cinemaproject.daos.CinemasDao;
import com.mycompany.cinemaproject.daos.RoomDao;
import com.mycompany.cinemaproject.datatypes.CinemaDTO;
import com.mycompany.cinemaproject.datatypes.MovieDTO;
import com.mycompany.cinemaproject.datatypes.RoomsDTO;
import com.mycompany.cinemaproject.datatypes.RowsDTO;
import com.mycompany.cinemaproject.entities.Cinemas;
import com.mycompany.cinemaproject.entities.Rooms;
import com.mycompany.cinemaproject.entities.Rows;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author SKCADMIN
 */
@Component
public class CinemaService {

    @PersistenceContext
    EntityManager em;

    @Autowired
    CinemasDao cDao;

    @Autowired
    RowsDao rDao;

    @Autowired
    RoomDao roomDao;

    public List<CinemaDTO> returnCinemas() {
        List<CinemaDTO> dTO = new ArrayList();

        List<Cinemas> cData = cDao.findAll();
        for (Cinemas cinemas : cData) {
            CinemaDTO cCD = new CinemaDTO(cinemas);
            dTO.add(cCD);
        }

        return dTO;
    }

    public boolean deleteCinema(Long id) {
        try {
            cDao.delete(id);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Transactional
    public CinemaDTO modifyCinemas(Long id, CinemaDTO cDTO) {
        Cinemas c = cDao.findOne(id);

        c.setAddress(cDTO.getAddress());
        c.setBase_ticket_price(cDTO.getBase_ticket_price());
        c.setDescription(cDTO.getDescription());
        c.setEmail(cDTO.getEmail());
        c.setGps_lat(cDTO.getGps_lat());
        c.setGps_lng(cDTO.getGps_lng());
        c.setName(cDTO.getName());
        c.setPhone(cDTO.getName());

        cDao.save(c);

        cDTO = new CinemaDTO(c);

        return cDTO;

    }

    @Transactional
    public RowsDTO addNewRowToRoom(Long id, RowsDTO rowDTO) {

        Rooms room = roomDao.findOne(id);
        em.persist(room);
        Rows r = new Rows(rowDTO);
        em.persist(r);
        r.setRoom(room);
        em.flush();
        rowDTO = new RowsDTO(r);

        return rowDTO;
    }

    @Transactional
    public CinemaDTO addNewCinema(CinemaDTO cinemadto) {
        Cinemas cinemaToDatabase = new Cinemas(cinemadto);

        em.persist(cinemaToDatabase);

        List<Cinemas> temp = cDao.findByName(cinemaToDatabase.getName());
        cinemadto = new CinemaDTO(temp.get(0));
        return cinemadto;
    }

    @Transactional
    public RoomsDTO addNewRoom(Long id, String name) {
        Cinemas c = cDao.findOne(id);
        Rooms room = new Rooms();

        em.persist(room);
        room.setCinema(c);
        room.setRoomName(name);
        RoomsDTO rDTO = new RoomsDTO(room);
        return rDTO;
    }

    @Transactional
    public List<RoomsDTO> showRooms(Long id) {
        List<Rooms> roomsList = roomDao.findByCinema_id(id);
        List<RoomsDTO> roomsListDTO = new ArrayList<>();
        for (Rooms room : roomsList) {
            RoomsDTO rooms = new RoomsDTO(room);
            roomsListDTO.add(rooms);

        }
        return roomsListDTO;

    }

    @Transactional
    public List<RoomsDTO> showAllRooms() {
        List<Rooms> roomsList = roomDao.findAll();
        List<RoomsDTO> roomsListDTO = new ArrayList<>();
        for (Rooms room : roomsList) {
            RoomsDTO rooms = new RoomsDTO(room);
            roomsListDTO.add(rooms);

        }
        return roomsListDTO;
    }

    @Transactional
    public CinemaDTO getOneCinema(Long id) {
        CinemaDTO cDTO = new CinemaDTO(cDao.findOne(id));
        return cDTO;
    }

    public RoomsDTO modifyRoom(Long roomid, String name) {
        Rooms room = roomDao.findOne(roomid);

        room.setRoomName(name);
        roomDao.save(room);

        return new RoomsDTO(room);

    }

    public RoomsDTO deleteRoom(Long roomid) {
        Rooms rooms = roomDao.findOne(roomid);
        roomDao.delete(roomid);
        RoomsDTO roomsDTO = new RoomsDTO(rooms);
        return roomsDTO;
    }
    
    public RowsDTO deleteRow(Long rowsID) {
        Rows row = rDao.findOne(rowsID);
        RowsDTO rowsDTO = new RowsDTO(row);
        rDao.delete(rowsID);
        return rowsDTO;

    }

    public RoomsDTO showOneRoom(Long roomID) {
        Rooms room = roomDao.findOne(roomID);
        RoomsDTO roomDTO = new RoomsDTO(room);

        return roomDTO;
    }

    public List<RowsDTO> showAllRows(Long roomID) {
        List<Rows> list = em.createQuery("Select r from Rows r join r.room t where t.id=:roomID")
                .setParameter("roomID", roomID)
                .getResultList();
        List<RowsDTO> rowsDTOList = new ArrayList<>();
        for (Rows rows : list) {
            RowsDTO rowDTO = new RowsDTO(rows);
            rowsDTOList.add(rowDTO);

        }

        return rowsDTOList;
    }

    public RowsDTO modifyRow(Long rowID, RowsDTO rowDTO) {
        Rows row = rDao.findOne(rowID);
        row.setIs_sofa(rowDTO.isIs_sofa());
        row.setSeats(rowDTO.getSeats());
        rDao.save(row);
        rowDTO = new RowsDTO(row);
        return rowDTO;
    }
    
    @Transactional
    public void fixRowNumbersOfRoom(Long id){
        EntityGraph eg = em.getEntityGraph("roomWithRows");
        Rooms room = em.createQuery("select r from Rooms r where id=:roomid", Rooms.class)
                .setParameter("roomid", id)
                .setHint("javax.persistence.loadgraph", eg)
                .getSingleResult();
        for (int i = 0; i < room.getRows().size(); i++) {
            room.getRows().get(i).setRowNumber(i + 1);
        }
        em.flush();
    }

}
