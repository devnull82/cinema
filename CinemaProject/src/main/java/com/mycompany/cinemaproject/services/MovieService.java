/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.mycompany.cinemaproject.daos.MovieDao;
import com.mycompany.cinemaproject.datatypes.CinemaAndShowDTO;
import com.mycompany.cinemaproject.datatypes.CinemaDTO;
import com.mycompany.cinemaproject.datatypes.MovieDTO;
import com.mycompany.cinemaproject.datatypes.TMDBGenre;
import com.mycompany.cinemaproject.entities.Categories;
import com.mycompany.cinemaproject.datatypes.ShowDTO;
import com.mycompany.cinemaproject.datatypes.TMDBMovieData;
import com.mycompany.cinemaproject.entities.Cinemas;
import com.mycompany.cinemaproject.entities.Movies;
import com.mycompany.cinemaproject.entities.Shows;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author OKTATAS
 */
@Component
public class MovieService {

    @Autowired
    MovieDao md;

    @Autowired
    MovieService ms;

    @PersistenceContext
    EntityManager em;

    public List<MovieDTO> getAllMovies() {
        List<Movies> movieList = md.findAll();
        List<MovieDTO> movieDtoList = new LinkedList<>();
        for (Movies movie : movieList) {
            movieDtoList.add(new MovieDTO(movie));
        }
        return movieDtoList;
    }

    public MovieDTO getMovie(Long id) {
        return new MovieDTO(md.findOne(id));
    }

    public Long deleteMovie(Long id) {
        md.delete(id);
        return id;
    }

    @Transactional
    public List<MovieDTO> getMoviesWhereCinemaShowsThem(Long cinemaId) {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        List<Movies> movieList = em.createQuery(
                "SELECT distinct m FROM Movies m join m.show s join s.room.cinema  c where c.id=:cinemaId "
                        + "and s.timestamp >:currentTime").
                setParameter("cinemaId", cinemaId).
                setParameter("currentTime", timestamp).
                getResultList();
        List<MovieDTO> moviesDTO = new ArrayList<>();
        for (Movies movies : movieList) {
            MovieDTO movieDTO = new MovieDTO(movies);
            moviesDTO.add(movieDTO);
        }
        return moviesDTO;
    }

    @Transactional
    public List<CinemaAndShowDTO> reservationHelperGetCinemasAndShows(Long movieID) {

        List<Cinemas> cinemaList = em.createQuery(
                "SELECT distinct c FROM Cinemas c join c.rooms r join r.show s join s.movie m"
                + " where m.id=:movieID").
                setParameter("movieID", movieID).
                getResultList();

        List<CinemaAndShowDTO> cinemaAndShowDTOList = new ArrayList<>();

        Timestamp timestamp = new Timestamp(new Date().getTime());

        for (Cinemas cinemas : cinemaList) {
            List<ShowDTO> showDtoList = new ArrayList<>();
            CinemaDTO cdto = new CinemaDTO(cinemas);
            List<Shows> showsList = em.createQuery(
                    "SELECT s FROM Cinemas c join c.rooms r join r.show s join s.movie m"
                    + " where c.id=:cinemaID and s.timestamp >:currentTime and m.id=:movieID").
                    setParameter("cinemaID", cdto.getId()).
                    setParameter("currentTime", timestamp).
                    setParameter("movieID", movieID).
                    getResultList();

            for (Shows shows : showsList) {
                ShowDTO showdto = new ShowDTO(shows);
                showDtoList.add(showdto);

            }

            CinemaAndShowDTO ctdoSdto = new CinemaAndShowDTO(cdto, showDtoList);
            cinemaAndShowDTOList.add(ctdoSdto);
        }
        return cinemaAndShowDTOList;
    }

    @Transactional
    public Long addMovie(MovieDTO movie) {
        Movies newMovie = new Movies();
        em.persist(newMovie);
        em.flush();
        movie.setId(newMovie.getId());
        ms.modifyMovie(movie);
        return movie.getId();
    }
    
    public MovieDTO getTMDBMovie(Long id){
        RestTemplate rt = new RestTemplate();
        TMDBMovieData tmdbmovie = rt.getForObject("https://api.themoviedb.org/3/movie/" + id + "?api_key=d5aa952e734358071b9dcb1f47b320a5"
                + "&language=en-US&append_to_response=videos,credits", TMDBMovieData.class);
        Movies movie = new Movies(tmdbmovie);
        return new MovieDTO(movie);
    }

    @Transactional
    public MovieDTO modifyMovie(MovieDTO newData) {
        Movies oldData = em.find(Movies.class, newData.getId());
        oldData.setActors(newData.getActors());
        oldData.setBackground_image_url(newData.getBackground_image_url());
        oldData.setDescription(newData.getDescription());
        oldData.setDirector(newData.getDirector());
        oldData.setHero_image_url(newData.getHero_image_url());
        oldData.setImdb_rating(newData.getImdb_rating());
        oldData.setIs_featured_in_slider(newData.isIs_featured_in_slider());
        oldData.setLength(newData.getLength());
        oldData.setRated(newData.getRated());
        oldData.setRelease_date(newData.getRelease_date());
        oldData.setThumbnail_url(newData.getThumbnail_url());
        oldData.setTitle(newData.getTitle());
        oldData.setTrailer_url(newData.getTrailer_url());
        oldData.getCategories().clear();
        ms.addCategories(oldData, newData.getCategoryList());
        em.flush();
        return new MovieDTO(oldData);
    }

    @Transactional
    public void addCategories(Movies movie, List<String> genres) {
        for (String genre : genres) {
            List<Categories> category = em.createQuery("select c from Categories c where c.name=:categoryname")
                    .setParameter("categoryname", genre)
                    .getResultList();
            if (category.isEmpty()) {
                Categories newCategory = new Categories(genre);
                em.persist(newCategory);
                movie.getCategories().add(newCategory);
            } else {
                movie.getCategories().add(category.get(0));
            }
        }
    }
    
    public List<String> tmdbGenresToStrings(List<TMDBGenre> tmdbGenreList){
        List<String> stringCategories = new LinkedList<>();
        for (TMDBGenre tMDBGenre : tmdbGenreList) {
            stringCategories.add(tMDBGenre.getName());
        }
        return stringCategories;
    }
}
