/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.mycompany.cinemaproject.daos.ShowDao;
import com.mycompany.cinemaproject.datatypes.ShowDTO;
import com.mycompany.cinemaproject.entities.Movies;
import com.mycompany.cinemaproject.entities.Rooms;
import com.mycompany.cinemaproject.entities.Shows;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author OKTATAS
 */
@Component
public class ShowService {

    @Autowired
    ShowDao sd;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public List<ShowDTO> getShows(Long cinemaId, Timestamp day, Long movieId) {
        List<Shows> showsList;
        if(cinemaId != -1){
            if (day.getTime() != -3600000) {
                if (movieId != -1) {
                    showsList = em.createQuery("select s from Cinemas c join c.rooms r join r.show s join s.movie m"
                            + " where c.id=:cinemaId and s.timestamp>=:daystart and s.timestamp<:dayend and"
                            + " m.id=:movieId")
                    .setParameter("cinemaId", cinemaId)
                    .setParameter("daystart", day)
                    .setParameter("dayend", new Timestamp(day.getTime() + 86400000))
                    .setParameter("movieId", movieId)
                    .getResultList();
                } else {
                    showsList = em.createQuery("select s from Cinemas c join c.rooms r join r.show s"
                            + " where c.id=:cinemaId and s.timestamp>=:daystart and s.timestamp<:dayend")
                    .setParameter("cinemaId", cinemaId)
                    .setParameter("daystart", day)
                    .setParameter("dayend", new Timestamp(day.getTime() + 86400000))
                    .getResultList();
                }
            } else {
                if(movieId != -1) {
                    showsList = em.createQuery("select s from Cinemas c join c.rooms r join r.show s join s.movie m"
                            + " where c.id=:cinemaId and s.timestamp >:currentTime and m.id=:movieId")
                    .setParameter("cinemaId", cinemaId)
                    .setParameter("currentTime", new Timestamp(new Date().getTime()))
                    .setParameter("movieId", movieId)
                    .getResultList();
                } else {
                    showsList = em.createQuery("select s from Cinemas c join c.rooms r join r.show s"
                            + " where c.id=:cinemaId and s.timestamp >:currentTime")
                    .setParameter("cinemaId", cinemaId)
                    .setParameter("currentTime", new Timestamp(new Date().getTime()))
                    .getResultList();
                }
            }
        } else {
            if (day.getTime() != -3600000) {
                if (movieId != -1) {
                    showsList = em.createQuery("select s from Shows s join s.movie m where s.timestamp>=:daystart and s.timestamp<:dayend"
                            + " and m.id=:movieId")
                    .setParameter("daystart", day)
                    .setParameter("dayend", new Timestamp(day.getTime() + 86400000))
                    .setParameter("movieId", movieId)
                    .getResultList();
                } else {
                    showsList = em.createQuery("select s from Shows s where s.timestamp>=:daystart and s.timestamp<:dayend")
                    .setParameter("daystart", day)
                    .setParameter("dayend", new Timestamp(day.getTime() + 86400000))
                    .getResultList();
                }
            } else {
                if (movieId != -1) {
                    showsList = em.createQuery("select s from Shows s join s.movie m where m.id=:movieId")
                    .setParameter("movieId", movieId)
                    .getResultList();
                } else {
                    showsList = sd.findAll();
                }
            }
        } 
        List<ShowDTO> showsDTOList = new ArrayList<>();
        for (Shows show : showsList) {
            showsDTOList.add(new ShowDTO(show));
        }
        return showsDTOList;
    }

    public ShowDTO getShow(Long id) {
        return new ShowDTO(sd.findOne(id));
    }

    @Transactional
    public ShowDTO addShow(ShowDTO show) {
        Shows showEntity = new Shows();
        em.persist(showEntity);
        showEntity.setIs_premiere(show.isIs_premiere());
        showEntity.setMovie(em.find(Movies.class, show.getMovie_id()));
        showEntity.setTimestamp(show.getTimestamp());
        showEntity.setRoom(em.find(Rooms.class, show.getRoom_id()));
        return new ShowDTO(showEntity);
    }

    @Transactional
    public Long deleteShow(Long id) {

        Shows show = (sd.findOne(id));

        sd.delete(id);

        return id;
    }
}
