package com.mycompany.cinemaproject.entities;

import com.mycompany.cinemaproject.datatypes.RowsDTO;
import java.util.List;
import static javax.persistence.CascadeType.REMOVE;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Rows {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; 
    
    private Integer seats;
    
    private Integer rowNumber;
    
    private boolean is_sofa;
    
    @ManyToOne
    private Rooms room;
    
    @OneToMany(mappedBy = "row",cascade=REMOVE)
    List<Reservations> reservation;

    public Rows() {
    }

    public Rows(RowsDTO rowDTO) {
       this.id = rowDTO.getId();
        this.seats = rowDTO.getSeats();
        this.is_sofa = rowDTO.isIs_sofa();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public boolean isIs_sofa() {
        return is_sofa;
    }

    public void setIs_sofa(boolean is_sofa) {
        this.is_sofa = is_sofa;
    }

    public Rooms getRoom() {
        return room;
    }

    public void setRoom(Rooms room) {
        this.room = room;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

}
