/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.entities;

import com.mycompany.cinemaproject.datatypes.ShowDTO;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author SKCADMIN
 */
@Entity
public class Shows {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private boolean is_premiere;
    private Timestamp timestamp;

    @OneToMany(mappedBy = "user")
    private List<Reservations> reservations;

    @ManyToOne
    private Movies movie;

    @ManyToOne
    private Rooms room;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isIs_premiere() {
        return is_premiere;
    }

    public void setIs_premiere(boolean is_premiere) {
        this.is_premiere = is_premiere;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public List<Reservations> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservations> reservations) {
        this.reservations = reservations;
    }

    public Movies getMovie() {
        return movie;
    }

    public void setMovie(Movies movie) {
        this.movie = movie;
    }

    public Rooms getRoom() {
        return room;
    }

    public void setRoom(Rooms room) {
        this.room = room;
    }

    public Shows() {
    }

    public Shows(Long id, boolean is_premiere, Timestamp timestamp, List<Reservations> reservations, Movies movie, Rooms room) {
        this.id = id;
        this.is_premiere = is_premiere;
        this.timestamp = timestamp;
        this.reservations = reservations;
        this.movie = movie;
        this.room = room;
    }
    

}
