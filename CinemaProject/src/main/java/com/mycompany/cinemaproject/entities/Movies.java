package com.mycompany.cinemaproject.entities;

import com.mycompany.cinemaproject.datatypes.Rating;
import com.mycompany.cinemaproject.datatypes.TMDBCrew;
import com.mycompany.cinemaproject.datatypes.TMDBMovieData;
import com.mycompany.cinemaproject.datatypes.TMDBVideo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author OKTATAS
 */
@Entity
public class Movies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String title;
    
    @Column(length = 1000)
    String description;

    Long length;

    Rating rated;

    String director;

    String actors;

    Double imdb_rating;

    String trailer_url;

    String thumbnail_url;
    
    String hero_image_url;
    
    boolean is_featured_in_slider;
    
    String background_image_url;
    
    Date release_date;

    @ManyToMany(fetch = FetchType.EAGER)
    List<Categories> categories = new ArrayList<>();

    @OneToMany(mappedBy = "movie")
    List<Shows> show = new ArrayList<>();

    public Movies(TMDBMovieData tmdbmovie) {
        this.title = tmdbmovie.getTitle();
        this.description = tmdbmovie.getOverview();
        this.length = tmdbmovie.getRuntime();
        this.release_date = tmdbmovie.getRelease_date();
        String actorString = "";
        for (int i = 0; i < 3; i++) {
            actorString = actorString + tmdbmovie.getCredits().getCast().get(i).getName();
            if (i < 2) {
                actorString = actorString + ", ";
            }
        }
        this.actors = actorString;
        for (TMDBCrew crewMember : tmdbmovie.getCredits().getCrew()) {
            if (crewMember.getJob().equals("Director")) {
                this.director = crewMember.getName();
                break;
            }
        }
        for (TMDBVideo video : tmdbmovie.getVideos().getResults()) {
            if (video.getType().equals("Trailer") && video.getSite().equals("YouTube")) {
                this.trailer_url = "https://www.youtube.com/embed/" + video.getKey();
            }
        }
        this.thumbnail_url = "http://image.tmdb.org/t/p/w185/" + tmdbmovie.getPoster_path();
        this.hero_image_url = "http://image.tmdb.org/t/p/original/" + tmdbmovie.getBackdrop_path();
        this.background_image_url = "http://image.tmdb.org/t/p/w500/" + tmdbmovie.getPoster_path();
    }

    public Date getRelease_date() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Rating getRated() {
        return rated;
    }

    public void setRated(Rating rated) {
        this.rated = rated;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public Double getImdb_rating() {
        return imdb_rating;
    }

    public void setImdb_rating(Double imdb_rating) {
        this.imdb_rating = imdb_rating;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public Movies() {
    }

    public List<Shows> getShow() {
        return show;
    }

    public void setShow(List<Shows> show) {
        this.show = show;
    }

    public String getHero_image_url() {
        return hero_image_url;
    }

    public void setHero_image_url(String hero_image_url) {
        this.hero_image_url = hero_image_url;
    }

    public boolean isIs_featured_in_slider() {
        return is_featured_in_slider;
    }

    public void setIs_featured_in_slider(boolean is_featured_in_slider) {
        this.is_featured_in_slider = is_featured_in_slider;
    }

    public String getBackground_image_url() {
        return background_image_url;
    }

    public void setBackground_image_url(String background_image_url) {
        this.background_image_url = background_image_url;
    }
}
