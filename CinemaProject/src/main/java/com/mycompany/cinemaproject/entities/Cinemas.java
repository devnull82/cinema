package com.mycompany.cinemaproject.entities;

import com.mycompany.cinemaproject.datatypes.CinemaDTO;
import java.util.ArrayList;
import java.util.List;
import static javax.persistence.CascadeType.REMOVE;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Cinemas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    private String name;

    private String description;

    private String address;

    private Double gps_lat;

    private Double gps_lng;

    private String email;

    private String phone;

    private Integer base_ticket_price;

    @OneToMany(cascade = REMOVE, mappedBy = "cinema", fetch = FetchType.LAZY)
    private List<Rooms> rooms = new ArrayList<>();

    public Cinemas() {
    }

    public Cinemas(CinemaDTO cinemadto) {
       this.name = cinemadto.getName();
       this.description = cinemadto.getDescription();
       this.address = cinemadto.getAddress();
       this.gps_lat = cinemadto.getGps_lat();
       this.gps_lng = cinemadto.getGps_lng();
       this.email = cinemadto.getEmail();
       this.phone = cinemadto.getPhone();
       this.base_ticket_price = cinemadto.getBase_ticket_price();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getGps_lat() {
        return gps_lat;
    }

    public void setGps_lat(Double gps_lat) {
        this.gps_lat = gps_lat;
    }

    public Double getGps_lng() {
        return gps_lng;
    }

    public void setGps_lng(Double gps_lng) {
        this.gps_lng = gps_lng;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getBase_ticket_price() {
        return base_ticket_price;
    }

    public void setBase_ticket_price(Integer base_ticket_price) {
        this.base_ticket_price = base_ticket_price;
    }

    public List<Rooms> getRooms() {
        return rooms;
    }

    public void setRooms(List<Rooms> rooms) {
        this.rooms = rooms;
    }

}
