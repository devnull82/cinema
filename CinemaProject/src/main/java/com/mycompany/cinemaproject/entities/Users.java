/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.entities;

import com.mycompany.cinemaproject.datatypes.RegistrationModes;
import com.mycompany.cinemaproject.datatypes.UserDTO;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author SKCADMIN
 */
@Entity

@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "userWithAuthorities",
            attributeNodes = {
                @NamedAttributeNode(value = "email")
                , @NamedAttributeNode(value = "password")
                ,
                @NamedAttributeNode(value = "authorities")
            })
})

public class Users implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    @Column(length = 1000)
    private String password;
    private String full_name;
    private String verify_hash;
    private boolean is_verified;
    private Timestamp timestamp;
    private RegistrationModes registration_mode;
    @Lob
    @Column(length = 100000)
    private byte[] file;

    @ManyToMany(targetEntity = Authorities.class, mappedBy = "user")
    private Collection<Authorities> authorities = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Reservations> reservations;

    public Users(Long id, String email, String password, String full_name, String verify_hash, boolean is_verified, Timestamp timestamp, RegistrationModes registration_mode, Collection<Authorities> authorities, List<Reservations> reservations, byte[] picture) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.full_name = full_name;
        this.verify_hash = verify_hash;
        this.is_verified = is_verified;
        this.timestamp = timestamp;
        this.registration_mode = registration_mode;
        this.authorities = authorities;
        this.reservations = reservations;
        this.file = picture;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] picture) {
        this.file = picture;
    }

    public Users() {
    }

    public Users(UserDTO u) {
        this.id = u.getId();
        this.email = u.getEmail();
        this.password = u.getPassword();
        this.full_name = u.getFull_name();
        this.verify_hash = u.getVerify_hash();
        this.is_verified = u.isIs_verified();
        this.timestamp = u.getTimestamp();
        this.registration_mode = u.getRegistration_mode();
        this.authorities = u.getAuthorities();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getVerify_hash() {
        return verify_hash;
    }

    public void setVerify_hash(String verify_hash) {
        this.verify_hash = verify_hash;
    }

    public boolean isIs_verified() {
        return is_verified;
    }

    public void setIs_verified(boolean is_verified) {
        this.is_verified = is_verified;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public RegistrationModes getRegistration_mode() {
        return registration_mode;
    }

    public void setRegistration_mode(RegistrationModes registration_mode) {
        this.registration_mode = registration_mode;
    }

    public List<Reservations> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservations> reservations) {
        this.reservations = reservations;
    }

    public Collection<Authorities> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<Authorities> authorities) {
        this.authorities = authorities;
    }

    public void addAuthorities(Authorities auth) {
        this.authorities.add(auth);
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
