/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.entities;

import com.mycompany.cinemaproject.datatypes.PaymentModes;
import com.mycompany.cinemaproject.datatypes.ReservationDTO;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;

/**
 *
 * @author SKCADMIN
 */
@Entity

@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "reservationGraph",
            attributeNodes = {
                @NamedAttributeNode(value = "show"), @NamedAttributeNode(value = "row")
            })
})
   

public class Reservations {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer seat;
    private Timestamp timestampReserved;
    private Timestamp timestampPaid;
    private PaymentModes payment_mode;
    
    @Lob
    private byte[] eticket;

    public byte[] getEticket() {
        return eticket;
    }

    public void setEticket(byte[] eticket) {
        this.eticket = eticket;
    }

    @ManyToOne
    private Shows show;

    @ManyToOne
    private Users user;
    
    @ManyToOne
    private Rows row;

    public Long getId() {
        return id;
    }  

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public Timestamp getTimestampReserved() {
        return timestampReserved;
    }

    public void setTimestampReserved(Timestamp timestamp_reserved) {
        this.timestampReserved = timestamp_reserved;
    }

    public Timestamp getTimestamp_paid() {
        return timestampPaid;
    }

    public void setTimestamp_paid(Timestamp timestamp_paid) {
        this.timestampPaid = timestamp_paid;
    }

    public PaymentModes getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(PaymentModes payment_mode) {
        this.payment_mode = payment_mode;
    }

    public Shows getShow() {
        return show;
    }

    public void setShow(Shows show) {
        this.show = show;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Rows getRow() {
        return row;
    }

    public void setRow(Rows row) {
        this.row = row;
    }

    public Reservations() {
    }

    public Reservations(Long id, Integer seat, Timestamp timestamp_reserved, Timestamp timestamp_paid, PaymentModes payment_mode, Shows show, Users user, Rows row) {
        this.id = id;
        this.seat = seat;
        this.timestampReserved = timestamp_reserved;
        this.timestampPaid = timestamp_paid;
        this.payment_mode = payment_mode;
        this.show = show;
        this.user = user;
        this.row = row;
    }
}
