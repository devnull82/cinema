package com.mycompany.cinemaproject.entities;

import java.util.ArrayList;
import java.util.List;
import static javax.persistence.CascadeType.REMOVE;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(name = "roomWithRows",
            attributeNodes = {
                @NamedAttributeNode(value = "rows")
            })
})
public class Rooms {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String roomName;
    
    

    @OneToMany(cascade = REMOVE, mappedBy = "room", fetch = FetchType.LAZY)
    private List<Rows> rows = new ArrayList<>();

    @ManyToOne
    private Cinemas cinema;

    @OneToMany(mappedBy = "room", cascade = REMOVE)
    List<Shows> show;

    public Rooms() {
    }

    public Rooms(Long id, String roomName, Cinemas cinema, List<Shows> show) {
        this.id = id;
        this.roomName = roomName;
        this.cinema = cinema;
        this.show = show;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public List<Rows> getRows() {
        return rows;
    }

    public void setRows(List<Rows> rows) {
        this.rows = rows;
    }

    public Cinemas getCinema() {
        return cinema;
    }

    public void setCinema(Cinemas cinema) {
        this.cinema = cinema;
    }

    public List<Shows> getShow() {
        return show;
    }

    public void setShow(List<Shows> show) {
        this.show = show;
    }

}
