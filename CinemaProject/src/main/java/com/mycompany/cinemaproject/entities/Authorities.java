package com.mycompany.cinemaproject.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import org.springframework.security.core.GrantedAuthority;


/**
 *
 * @author Krisz
 */

@Entity
public class Authorities implements GrantedAuthority {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToMany
    @JsonIgnore
    private List<Users> user = new ArrayList<>();
    
    String role;

    public Authorities(List<Users> user, String role) {
        this.user = user;
        this.role = role;
    }
    
    public Authorities(Users user, String role) {
        this.user.add(user);
        this.role = role;
    }

    public Authorities() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Users> getUser() {
        return user;
    }

    public void setUser(List<Users> user) {
        this.user = user;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }
    
    
    
}