/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.exceptions;

/**
 *
 * @author Krisz
 */
public class UserAlreadyRegisteredException extends Exception {

    public UserAlreadyRegisteredException(String msg) {
        super(msg);
    }
}
