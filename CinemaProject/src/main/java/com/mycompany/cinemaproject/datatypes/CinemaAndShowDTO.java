/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import java.util.List;

/**
 *
 * @author SKCADMIN
 */
public class CinemaAndShowDTO {

    CinemaDTO cinemaDTO;
    List<ShowDTO>showsDTO;

    public CinemaAndShowDTO(CinemaDTO cinemaDTO, List<ShowDTO> showsDTO) {
        this.cinemaDTO = cinemaDTO;
        this.showsDTO = showsDTO;
    }

    public CinemaAndShowDTO() {
    }
 

    public CinemaDTO getCinemaDTO() {
        return cinemaDTO;
    }

    public void setCinemaDTO(CinemaDTO cinemaDTO) {
        this.cinemaDTO = cinemaDTO;
    }

    public List<ShowDTO> getShowsDTO() {
        return showsDTO;
    }

    public void setShowsDTO(List<ShowDTO> showsDTO) {
        this.showsDTO = showsDTO;
    }
    
    
}
