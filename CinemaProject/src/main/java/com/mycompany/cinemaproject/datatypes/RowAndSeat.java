package com.mycompany.cinemaproject.datatypes;

/**
 *
 * @author Krisz
 */
public class RowAndSeat {

    private Long rowId;
    private Integer rowNumber;
    private Integer seat;

    public RowAndSeat() {
    }

    public Long getRowId() {
        return rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public RowAndSeat(Integer rowNumber, Integer seat,Long rowId) {
        this.rowId = rowId;
        this.rowNumber = rowNumber;
        this.seat = seat;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

}