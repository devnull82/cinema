/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Categories;

/**
 *
 * @author OKTATAS
 */
class CategoryDTO {
    Long id;
    
    String name;

    public CategoryDTO(){
        
    }
    
    public CategoryDTO(Categories category){
        this.id = category.getId();
        this.name = category.getName();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
