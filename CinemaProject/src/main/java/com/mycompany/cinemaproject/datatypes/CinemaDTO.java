/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Cinemas;

/**
 *
 * @author OKTATAS
 */
public class CinemaDTO {

    private Long id;

    private String name;

    private String description;

    private String address;

    private Double gps_lat;

    private Double gps_lng;

    private String email;

    private String phone;

    private Integer base_ticket_price;

    public CinemaDTO(Cinemas c) {

        this.id = c.getId();
        this.name = c.getName();
        this.description = c.getDescription();
        this.address = c.getAddress();
        this.gps_lat = c.getGps_lat();
        this.gps_lng = c.getGps_lng();
        this.email = c.getEmail();
        this.phone = c.getPhone();
        this.base_ticket_price = c.getBase_ticket_price();
    }

    public CinemaDTO() {
    }
    
    
    
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getGps_lat() {
        return gps_lat;
    }

    public void setGps_lat(Double gps_lat) {
        this.gps_lat = gps_lat;
    }

    public Double getGps_lng() {
        return gps_lng;
    }

    public void setGps_lng(Double gps_lng) {
        this.gps_lng = gps_lng;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getBase_ticket_price() {
        return base_ticket_price;
    }

    public void setBase_ticket_price(Integer base_ticket_price) {
        this.base_ticket_price = base_ticket_price;
    }

}
