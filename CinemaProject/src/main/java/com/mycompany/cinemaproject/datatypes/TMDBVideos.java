/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import java.util.List;

/**
 *
 * @author OKTATAS
 */
public class TMDBVideos {
    List<TMDBVideo> results;

    public List<TMDBVideo> getResults() {
        return results;
    }

    public void setResults(List<TMDBVideo> results) {
        this.results = results;
    }
    
    
}
