/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 *
 * @author OKTATAS
 */
public class TMDBMovieData {
    
    String backdrop_path;
    
    List<TMDBGenre> genres;
    
    String overview;
    
    String poster_path;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yy-MM-dd")
    Date release_date;
    
    Long runtime;
    
    String title;
    
    TMDBVideos videos;
    
    TMDBCredits credits;

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public List<TMDBGenre> getGenres() {
        return genres;
    }

    public void setGenres(List<TMDBGenre> genres) {
        this.genres = genres;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public Date getRelease_date() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    public Long getRuntime() {
        return runtime;
    }

    public void setRuntime(Long runtime) {
        this.runtime = runtime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TMDBVideos getVideos() {
        return videos;
    }

    public void setVideos(TMDBVideos videos) {
        this.videos = videos;
    }

    public TMDBCredits getCredits() {
        return credits;
    }

    public void setCredits(TMDBCredits credits) {
        this.credits = credits;
    }
    
    
}
