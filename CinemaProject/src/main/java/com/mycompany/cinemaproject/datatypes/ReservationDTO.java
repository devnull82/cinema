package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Reservations;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Krisz
 */
public class ReservationDTO {

    private Long id;
    private Timestamp timestamp_reserved;
    private Timestamp timestamp_paid;
    private PaymentModes payment_mode;
    private Long showId;
    private Long userId;
    private List<RowAndSeat> rowAndSeatList = new ArrayList<>();
    private String title;
    private Long price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public ReservationDTO() {
    }

    public ReservationDTO(Reservations reservation, List<RowAndSeat> rowAndSeatList) {
        this.rowAndSeatList = rowAndSeatList;
        this.payment_mode = reservation.getPayment_mode();
        this.showId = reservation.getShow().getId();
        this.userId = reservation.getUser().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getTimestamp_reserved() {
        return timestamp_reserved;
    }

    public void setTimestamp_reserved(Timestamp timestamp_reserved) {
        this.timestamp_reserved = timestamp_reserved;
    }

    public Timestamp getTimestamp_paid() {
        return timestamp_paid;
    }

    public void setTimestamp_paid(Timestamp timestamp_paid) {
        this.timestamp_paid = timestamp_paid;
    }

    public PaymentModes getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(PaymentModes payment_mode) {
        this.payment_mode = payment_mode;
    }

    public Long getShowId() {
        return showId;
    }

    public void setShowId(Long showId) {
        this.showId = showId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<RowAndSeat> getRowAndSeatList() {
        return rowAndSeatList;
    }

    public void setRowAndSeatList(List<RowAndSeat> rowAndSeatList) {
        this.rowAndSeatList = rowAndSeatList;
    }

}
