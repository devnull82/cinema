package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Authorities;
import com.mycompany.cinemaproject.entities.Users;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Krisz
 */
public class UserDTO {

    private Long id;
    private String email;
    private String password;
    private String full_name;
    private String verify_hash;
    private boolean is_verified;
    private Timestamp timestamp;
    private RegistrationModes registration_mode;
    private Collection<Authorities> authorities;
    private byte[] file;

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public UserDTO(Users u) {
        this.id = u.getId();
        this.email = u.getEmail();
        this.password = u.getPassword();
        this.full_name = u.getFull_name();
        this.verify_hash = u.getVerify_hash();
        this.is_verified = u.isIs_verified();
        this.timestamp = u.getTimestamp();
        this.registration_mode = u.getRegistration_mode();
        this.authorities = u.getAuthorities();
        this.file = u.getFile();
    }

    public UserDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getVerify_hash() {
        return verify_hash;
    }

    public void setVerify_hash(String verify_hash) {
        this.verify_hash = verify_hash;
    }

    public boolean isIs_verified() {
        return is_verified;
    }

    public void setIs_verified(boolean is_verified) {
        this.is_verified = is_verified;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public RegistrationModes getRegistration_mode() {
        return registration_mode;
    }

    public void setRegistration_mode(RegistrationModes registration_mode) {
        this.registration_mode = registration_mode;
    }

    public Collection<Authorities> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<Authorities> authorities) {
        this.authorities = authorities;
    }

}
