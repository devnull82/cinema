/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Categories;
import com.mycompany.cinemaproject.entities.Movies;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;

/**
 *
 * @author OKTATAS
 */
public class MovieDTO {
    
    Long id;
    
    String title;
    
    String description;
    
    Long length;
    
    Rating rated;
    
    String director;
    
    String actors;
    
    Double imdb_rating;
    
    String trailer_url;
    
    String thumbnail_url;
    
    String hero_image_url;
    
    boolean is_featured_in_slider;
    
    String background_image_url;
    

    List<String> categoryList = new LinkedList<>();

    Date release_date;


    public MovieDTO() {
    }

    public MovieDTO(Movies movie) {
        this.id = movie.getId();
        this.title = movie.getTitle();
        this.description = movie.getDescription();
        this.length = movie.getLength();
        this.rated = movie.getRated();
        this.director = movie.getDirector();
        this.actors = movie.getActors();
        this.imdb_rating = movie.getImdb_rating();
        this.trailer_url = movie.getTrailer_url();
        this.thumbnail_url = movie.getThumbnail_url();
        this.hero_image_url = movie.getHero_image_url();
        this.is_featured_in_slider =  movie.isIs_featured_in_slider();
        this.background_image_url = movie.getBackground_image_url();
        for (Categories category : movie.getCategories()) {
            this.categoryList.add(category.getName());
        }
        this.release_date = movie.getRelease_date();
    }

    public Date getRelease_date() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Rating getRated() {
        return rated;
    }

    public void setRated(Rating rated) {
        this.rated = rated;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public Double getImdb_rating() {
        return imdb_rating;
    }

    public void setImdb_rating(Double imdb_rating) {
        this.imdb_rating = imdb_rating;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getHero_image_url() {
        return hero_image_url;
    }

    public void setHero_image_url(String hero_image_url) {
        this.hero_image_url = hero_image_url;
    }

    public boolean isIs_featured_in_slider() {
        return is_featured_in_slider;
    }

    public void setIs_featured_in_slider(boolean is_featured_in_slider) {
        this.is_featured_in_slider = is_featured_in_slider;
    }

    public String getBackground_image_url() {
        return background_image_url;
    }

    public void setBackground_image_url(String background_image_url) {
        this.background_image_url = background_image_url;
    }

    public List<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<String> categoryList) {
        this.categoryList = categoryList;
    }
}
