/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Rows;

/**
 *
 * @author SKCADMIN
 */
public class RowsDTO {
    private Long id;
    
    private Long room_id;
    
    private Integer seats;
    
    private boolean is_sofa;
    
    private Integer rowNumber;

    public RowsDTO(Rows row) {
        this.id = row.getId();
        this.seats = row.getSeats();
        this.is_sofa = row.isIs_sofa();
        this.rowNumber = row.getRowNumber();
    }

    public RowsDTO() {
       
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Long room_id) {
        this.room_id = room_id;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public boolean isIs_sofa() {
        return is_sofa;
    }

    public void setIs_sofa(boolean is_sofa) {
        this.is_sofa = is_sofa;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }
    
}
