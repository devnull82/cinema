package com.mycompany.cinemaproject.datatypes;

/**
 *
 * @author Krisz
 */
public enum Roles {
    ROLE_USER, ROLE_ADMIN
}
