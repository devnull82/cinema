/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import java.util.List;

/**
 *
 * @author OKTATAS
 */
public class TMDBCredits {
    
    List<TMDBActor> cast;
    
    List<TMDBCrew> crew;

    public List<TMDBActor> getCast() {
        return cast;
    }

    public void setCast(List<TMDBActor> cast) {
        this.cast = cast;
    }

    public List<TMDBCrew> getCrew() {
        return crew;
    }

    public void setCrew(List<TMDBCrew> crew) {
        this.crew = crew;
    }
    
    
}
