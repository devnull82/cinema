/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Movies;
import com.mycompany.cinemaproject.entities.Reservations;
import com.mycompany.cinemaproject.entities.Rooms;
import com.mycompany.cinemaproject.entities.Shows;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author OKTATAS
 */
public class ShowDTO {
    
    private Long id;
    private boolean is_premiere;
    private Timestamp timestamp;
    private Long movie_id;
    private Long room_id;

    public ShowDTO(Shows show) {
        this.id = show.getId();
        this.is_premiere = show.isIs_premiere();
        this.timestamp =show.getTimestamp();
        this.movie_id = show.getMovie().getId();
        this.room_id = show.getRoom().getId();
    }

    public ShowDTO() {
    }   

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    

    public boolean isIs_premiere() {
        return is_premiere;
    }

    public void setIs_premiere(boolean is_premiere) {
        this.is_premiere = is_premiere;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Long getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(Long movie_id) {
        this.movie_id = movie_id;
    }

    public Long getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Long room_id) {
        this.room_id = room_id;
    }
    
    
}
