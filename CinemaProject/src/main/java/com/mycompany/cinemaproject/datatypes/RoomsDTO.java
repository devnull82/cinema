/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.datatypes;

import com.mycompany.cinemaproject.entities.Rooms;

/**
 *
 * @author SKCADMIN
 */
public class RoomsDTO {

    private Long cinema_id;

    private Long id;

    private String roomName;

    public RoomsDTO(Rooms room) {
        this.cinema_id = room.getCinema().getId();
        this.id = room.getId();
        this.roomName = room.getRoomName();
    }

    public RoomsDTO() {

    }

    public RoomsDTO(Long cinema_id, Long id, String roomName) {
        this.cinema_id = cinema_id;
        this.id = id;
        this.roomName = roomName;
    }

    public Long getCinema_id() {
        return cinema_id;
    }

    public void setCinema_id(Long cinema_id) {
        this.cinema_id = cinema_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

}
