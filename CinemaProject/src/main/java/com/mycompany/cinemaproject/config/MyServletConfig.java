package com.mycompany.cinemaproject.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = {/*TODO*/})
public class MyServletConfig extends WebMvcConfigurerAdapter{

    public static final String[] ALLOWED_CORS_ORIGINS = {"http://localhost:3000",
        "http://cinema.project.progmatic.hu",
        "https://www.sandbox.paypal.com/webapps",
    "http://www.cinema.project.progmatic.hu"};
    public static final String CORS_MAPPNIG = "/**";
    public static final String[] ALLOWED_HEADERS = {"*"};
    
    @Autowired
    ApplicationContext context;
    
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(CORS_MAPPNIG)
                .allowedOrigins(ALLOWED_CORS_ORIGINS)
                .allowedMethods("*")
                .allowedHeaders(ALLOWED_HEADERS)
                .allowCredentials(true);
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer conf) {
        conf.enable();
    }
    
   /*@Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("http://localhost:3000")
                .allowedMethods("PUT","GET","DELETE","POST")
                ;s

    }
    */
    
}