/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.daos.UserDao;
import com.mycompany.cinemaproject.exceptions.VerifyException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 *
 * @author SKCADMIN
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class VerifyServiceTest {

    @Autowired
    UserDao uDao;

    @Autowired
    VerifyService verifyService;

    public VerifyServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of verify method, of class VerifyService.
     *
     * @throws java.lang.Exception
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testVerify() throws Exception {
        System.out.println("verify");

        String email = "attila101710@gmail.com";
        String hash = "-86-259114-93-11822-6712182-7912259-109-126-88121-86-28-58-13-8-29424951305046-61098";

        try {
            verifyService.verify(3L, hash);
            assertTrue(true);
        } catch (VerifyException e) {
            if (e.getMessage().equals("Already Verified")) {
                assertTrue(true);
            }
            if (e.getMessage().equals("Hash Code mismatch")) {
                assertTrue(true);
            }
        }

    }

}
