/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.daos.CinemasDao;
import com.mycompany.cinemaproject.daos.RoomDao;
import com.mycompany.cinemaproject.daos.RowsDao;
import com.mycompany.cinemaproject.datatypes.CinemaDTO;
import com.mycompany.cinemaproject.datatypes.RoomsDTO;
import com.mycompany.cinemaproject.datatypes.RowsDTO;
import java.util.List;
import static java.util.Objects.isNull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 *
 * @author SKCADMIN
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class CinemaServiceTest {

    @PersistenceContext
    EntityManager em;

    @Autowired
    CinemaService cs;

    @Autowired
    CinemasDao cDao;

    @Autowired
    RowsDao rDao;

    @Autowired
    RoomDao roomDao;

    public CinemaServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of returnCinemas method, of class CinemaService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testReturnCinemas() {
        System.out.println("returnCinemas");

        List<CinemaDTO> result = cs.returnCinemas();
        assertTrue(!result.isEmpty());

    }

    /**
     * Test of deleteCinema method, of class CinemaService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testDeleteCinema() {
        System.out.println("deleteCinema");
        cs.deleteCinema(3L);
        try {
            cs.deleteCinema(-1L);
        } catch (Exception e) {
            assertTrue(true);
        }
        assertTrue(isNull(cDao.findOne(3L)));
    }

    /**
     * Test of modifyCinemas method, of class CinemaService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testModifyCinemas() {
        System.out.println("modifyCinemas");
        CinemaDTO c1 = cs.getOneCinema(3L);
        CinemaDTO cdto = cs.getOneCinema(7L);
        cs.modifyCinemas(3L, cdto);
        CinemaDTO c2 = cs.getOneCinema(3L);
        assertTrue(c1 != c2);
    }

    /**
     * Test of addNewRowToRoom method, of class CinemaService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    @Transactional
    public void testAddNewRowToRoom() {
        System.out.println("addNewRowToRoom");
        RowsDTO rowDTO = new RowsDTO();
        rowDTO.setIs_sofa(true);
        RowsDTO row = cs.addNewRowToRoom(1L, rowDTO);
        assertEquals(true, row.isIs_sofa());

    }

    /**
     * Test of addNewCinema method, of class CinemaService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    @Transactional
    public void testAddNewCinema() {
        System.out.println("addNewCinema");
        CinemaDTO cdtoMain = new CinemaDTO(cDao.findOne(3L));
        CinemaDTO cdto = cs.addNewCinema(cdtoMain);
        assertTrue(cdto.getId() != null);
    }

    /**
     * Test of addNewRoom method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testAddNewRoom() {
        System.out.println("addNewRoom");
        RoomsDTO room = cs.addNewRoom(3L, "Name");
        RoomsDTO expResult = room;
        assertEquals(expResult, room);
    }

    /**
     * Test of showRooms method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testShowRooms() {
        System.out.println("showRooms");
        List<RoomsDTO> expResult = cs.showRooms(3L);
        assertTrue(!expResult.isEmpty());
    }

    /**
     * Test of showAllRooms method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testShowAllRooms() {
        System.out.println("showAllRooms");
        List<RoomsDTO> roomsList = cs.showAllRooms();
        assertTrue(!roomsList.isEmpty());
    }

    /**
     * Test of getOneCinema method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testGetOneCinema() {
        System.out.println("getOneCinema");
        CinemaDTO expResult = new CinemaDTO();
        expResult.setName("mikkamakka");
        CinemaDTO result = cs.getOneCinema(3L);
        assertEquals(result.getName(), expResult.getName());
    }

    /**
     * Test of modifyRoom method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testModifyRoom() {
        System.out.println("modifyRoom");
        RoomsDTO roomDTO = new RoomsDTO();
        roomDTO.setRoomName("Name");
        RoomsDTO expResult = cs.modifyRoom(1L, "Name");
        assertEquals(expResult.getRoomName(), roomDTO.getRoomName());

    }

    /**
     * Test of deleteRoom method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testDeleteRoom() {
        System.out.println("deleteRoom");

        RoomsDTO expResult = cs.deleteRoom(1L);
        assertTrue(isNull(roomDao.findOne(1L)));

    }

    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testDeleteRow() {
        System.out.println("deleteRow");
        cs.deleteRow(1L);
        //em.flush();
        assertTrue(em.createQuery("Select r FROM Rows r where r.id=:id").setParameter("id", 1L).getResultList().isEmpty());
    }

    /**
     * Test of showOneRoom method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testShowOneRoom() {
        System.out.println("showOneRoom");

        RoomsDTO expResult = cs.showOneRoom(1L);
        assertTrue(!isNull(expResult));

    }

    /**
     * Test of showAllRows method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testShowAllRows() {
        System.out.println("showAllRows");
        List<RowsDTO> rowsList = cs.showAllRows(1L);
        assertEquals(1, rowsList.size());
    }

    /**
     * Test of modifyRow method, of class CinemaService.
     */
    @Transactional
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testModifyRow() {
        System.out.println("modifyRow");
        RowsDTO rowDTO = new RowsDTO();
        rowDTO.setIs_sofa(true);
        RowsDTO rowDTO2 = new RowsDTO(rDao.findOne(1L));
        rowDTO = cs.modifyRow(1L, rowDTO);
        assertTrue(rowDTO.isIs_sofa());

    }
}
