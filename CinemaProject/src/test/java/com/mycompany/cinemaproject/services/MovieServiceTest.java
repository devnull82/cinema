/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.datatypes.CinemaAndShowDTO;
import com.mycompany.cinemaproject.datatypes.MovieDTO;
import com.mycompany.cinemaproject.datatypes.TMDBGenre;
import com.mycompany.cinemaproject.entities.Movies;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 *
 * @author OKTATAS
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class MovieServiceTest {
    
    @PersistenceContext
    EntityManager em;
    
    @Autowired
    MovieService ms;
    
    public MovieServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAllMovies method, of class MovieService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testGetAllMovies() {
        List<MovieDTO> returnList = ms.getAllMovies();
        assertEquals("Akira", returnList.get(0).getTitle());
    }

    /**
     * Test of getMovie method, of class MovieService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testGetMovie() {
        MovieDTO returnMovie = ms.getMovie(10L);
        assertEquals("Daniel Kaluuya, Allison Williams, Bradley Whitford", returnMovie.getActors());
    }

    /**
     * Test of deleteMovie method, of class MovieService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testDeleteMovie() {
        assertTrue(1L == ms.deleteMovie(1L));
    }

    /**
     * Test of getMoviesWhereCinemaShowsThem method, of class MovieService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testGetMoviesWhereCinemaShowsThem() {
        List<MovieDTO> returnList = ms.getMoviesWhereCinemaShowsThem(3L);
        assertEquals(new Long(10), returnList.get(0).getId());
    }

    /**
     * Test of reservationHelperGetCinemasAndShows method, of class MovieService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testReservationHelperGetCinemasAndShows() {
        List<CinemaAndShowDTO> returnList = ms.reservationHelperGetCinemasAndShows(10L);
        assertEquals("mikkamakka", returnList.get(0).getCinemaDTO().getName());
        assertEquals(new Long(10), returnList.get(0).getShowsDTO().get(0).getMovie_id());
    }

    /**
     * Test of addMovie method, of class MovieService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testAddMovie() {
        MovieDTO movie = new MovieDTO();
        assertEquals(new Long(16), ms.addMovie(movie));
    }

    /**
     * Test of modifyMovie method, of class MovieService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testModifyMovie() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = "{\"id\":10,\"title\":\"Get Outtahere\",\"description\":\"Chris and his girlfriend Rose go upstate to visit her parents for the weekend. At first, Chris reads the family's overly accommodating behavior as nervous attempts to deal with their daughter's interracial relationship, but as the weekend progresses, a series of increasingly disturbing discoveries lead him to a truth that he never could have imagined.\",\"length\":104,\"rated\":null,\"director\":\"Jordan Peele\",\"actors\":\"Daniel Kaluuya, Allison Williams, Bradley Whitford\",\"imdb_rating\":null,\"trailer_url\":\"https://www.youtube.com/embed/sRfnevzM9kQ\",\"thumbnail_url\":\"http://image.tmdb.org/t/p/w185//1SwAVYpuLj8KsHxllTF8Dt9dSSX.jpg\",\"hero_image_url\":\"http://image.tmdb.org/t/p/original//Ae58bf7Yj6OPzwKerPgXSnxCJdh.jpg\",\"is_featured_in_slider\":false,\"background_image_url\":\"http://image.tmdb.org/t/p/w500//1SwAVYpuLj8KsHxllTF8Dt9dSSX.jpg\",\"categoryList\":[\"Mystery\",\"Thriller\",\"Kutya\",\"Horror\"],\"release_date\":1487894400000}";
        MovieDTO newData = mapper.readValue(jsonInString, MovieDTO.class);
        ms.modifyMovie(newData);
        assertEquals("Get Outtahere", ms.getMovie(10L).getTitle());
    }

    /**
     * Test of getTMDBMovie method, of class MovieService.
     */
    @Test
    public void testGetTMDBMovie() {
        assertEquals("Akira", ms.getTMDBMovie(149L).getTitle());
    }
}
