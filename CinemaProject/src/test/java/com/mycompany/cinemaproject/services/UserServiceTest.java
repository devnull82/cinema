/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.daos.UserDao;
import com.mycompany.cinemaproject.datatypes.UserDTO;
import com.mycompany.cinemaproject.entities.Users;
import com.mycompany.cinemaproject.mailSending.MailService;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import static java.util.Objects.isNull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 *
 * @author SKCADMIN
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class UserServiceTest {

    @PersistenceContext
    EntityManager em;

    @Autowired
    UserDao uDao;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MailService mailservice;

    @Autowired
    UserService userService;

    public UserServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addNewUser method, of class UserService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testAddNewUser() throws Exception {
        System.out.println("addNewUser");
        UserDTO userDto = new UserDTO();
        userDto.setFull_name("TesztName");
        userDto.setEmail("TesztEmail@TesztEmail.com");
        userService.addNewUser(userDto);
        userDto = new UserDTO(uDao.findByEmail("TesztEmail@TesztEmail.com"));
        assertEquals("TesztEmail@TesztEmail.com", userDto.getEmail());

    }

    /**
     * Test of generateVerifyHash method, of class UserService.
     *
     * @throws java.lang.Exception
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testGenerateVerifyHash() throws Exception {
        String input = (String.valueOf(Timestamp.valueOf(LocalDateTime.now())) + "AddMoreTextHere");
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(input.getBytes("UTF-8"));
        byte[] byteArray = digest.digest();
        String ret = "";
        for (int i = 0; i < byteArray.length; i++) {
            ret = ret + byteArray[i];
        }
        assertTrue(!isNull(ret));
    }

    /**
     * Test of findUserWithAuthorities method, of class UserService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testFindUserWithAuthorities() {
        System.out.println("findUserWithAuthorities");
        String email = "attila101710@gmail.com";
        Users user = userService.findUserWithAuthorities(email);
        assertTrue(!isNull(user.getAuthorities()));
    }

    /**
     * Test of findUser method, of class UserService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testFindUser() {
        System.out.println("findUser");
        UserDTO uDto = new UserDTO();
        uDto.setEmail("attila101710@gmail.com");
        Users user = userService.findUser(uDto);
        assertTrue(!isNull(user));
    }

}
