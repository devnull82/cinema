/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.services;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.datatypes.ShowDTO;
import com.mycompany.cinemaproject.entities.Shows;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 *
 * @author OKTATAS
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class ShowServiceTest {
    
    @PersistenceContext
    EntityManager em;
    
    @Autowired
    ShowService ss;
    
    public ShowServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getShows method, of class ShowService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testGetShows() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        Date d = sdf.parse("2017.12.25");
        List<ShowDTO> returnList;
        returnList = ss.getShows(-1L, new Timestamp(-3600000), -1L);
        assertEquals(new Long(10), returnList.get(0).getMovie_id());
        assertEquals(5, returnList.size());
        returnList = ss.getShows(-1L, new Timestamp(-3600000), 10L);
        assertEquals(4, returnList.size());
        returnList = ss.getShows(-1L, new Timestamp(d.getTime()), -1L);
        assertEquals(3, returnList.size());
        returnList = ss.getShows(-1L, new Timestamp(d.getTime()), 10L);
        assertEquals(3, returnList.size());
        returnList = ss.getShows(7L, new Timestamp(-3600000), -1L);
        assertEquals(2, returnList.size());
        returnList = ss.getShows(7L, new Timestamp(-3600000), 10L);
        assertEquals(2, returnList.size());
        returnList = ss.getShows(7L, new Timestamp(d.getTime()), -1L);
        assertEquals(1, returnList.size());
        returnList = ss.getShows(7L, new Timestamp(d.getTime()), 10L);
        assertEquals(1, returnList.size());
    }

    /**
     * Test of getShow method, of class ShowService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Test
    public void testGetShow() {
        assertEquals(new Long(11L), ss.getShow(6L).getMovie_id());
    }

    /**
     * Test of addShow method, of class ShowService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testAddShow() {
        ShowDTO newShow = new ShowDTO();
        newShow.setIs_premiere(false);
        newShow.setMovie_id(11L);
        newShow.setRoom_id(7L);
        assertEquals(new Long(7), ss.addShow(newShow).getId());
    }

    /**
     * Test of deleteShow method, of class ShowService.
     */
    @DatabaseSetup("moviesandcategories.xml")
    @Transactional
    @Test
    public void testDeleteShow() {
        assertEquals(new Long(6), ss.deleteShow(6L));
    }

}
