package com.mycompany.cinemaproject.TestConfig;

import com.mycompany.cinemaproject.daos.MovieDao;
import com.mycompany.cinemaproject.mailSending.MailService;
import com.mycompany.cinemaproject.services.CinemaService;
import com.mycompany.cinemaproject.services.MovieService;
import com.mycompany.cinemaproject.services.ShowService;
import com.mycompany.cinemaproject.services.UserService;
import com.mycompany.cinemaproject.services.VerifyService;
import javax.sql.DataSource;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author OKTATAS
 */
@Configuration
@EnableJpaRepositories(basePackageClasses = MovieDao.class)
public class TestConfig {

    CinemaService mock = Mockito.mock(CinemaService.class);

    /*@Bean
    public CinemaService CinemaServiceMock() {
        List<CinemaDTO> cinemaDTOList = new ArrayList<>();
        Mockito.when(mock.returnCinemas())
                .thenReturn(cinemaDTOList);
        return mock;
    }*/
 /*

    @Bean
    public UserOptions userOptions() {
        UserOptions mock = new UserOptions();
        return mock;
    }

    @Bean
    public UserOptions globalOptions() {
        UserOptions mock = new UserOptions();
        return mock;
    }

    @Bean
    public UserDetailsService userDetailsService() {
        JdbcUserDetailsManager mock = Mockito.mock(JdbcUserDetailsManager.class);
        return mock;
    }

    @Bean
    public SimpMessageSendingOperations simpMessageSendingOperations() {
        return Mockito.mock(SimpMessageSendingOperations.class);
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public MessageDao md() {
        return Mockito.mock(MessageDao.class);
    }

    @Bean
    public UserDao userDao() {
        return Mockito.mock(UserDao.class);
    }

    @Bean
    UserManager allUsers() {
        return new UserManager();
    }
     */
    @Bean
    PasswordEncoder passwordEncoder() {
        PasswordEncoder mocked = Mockito.mock(PasswordEncoder.class);
        return mocked;
    }

    @Bean
    MailService mailService() {
        MailService mocked = Mockito.mock(MailService.class);
        return mocked;
    }

    @Bean
    UserService userService() {
        return new UserService();
    }

    @Bean
    MovieService movieService() {
        return new MovieService();
    }

    @Bean
    CinemaService cinemaService() {
        return new CinemaService();
    }

    @Bean
    ShowService showService() {
        return new ShowService();
    }

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .build();
    }

    @Bean
    public VerifyService verifyService() {
        return new VerifyService();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean emfb
                = new LocalContainerEntityManagerFactoryBean();
        emfb.setDataSource(dataSource);
        emfb.setJpaVendorAdapter(jpaVendorAdapter);
        emfb.setPackagesToScan("com.mycompany.cinemaproject.entities");
        return emfb;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.HSQL);
        adapter.setShowSql(true);
        adapter.setGenerateDdl(true);
        adapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");
        return adapter;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        return jpaTransactionManager;
    }
}
