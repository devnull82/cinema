
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.datatypes.CinemaAndShowDTO;
import com.mycompany.cinemaproject.datatypes.CinemaDTO;
import com.mycompany.cinemaproject.datatypes.MovieDTO;
import com.mycompany.cinemaproject.datatypes.ShowDTO;
import com.mycompany.cinemaproject.services.MovieService;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author OKTATAS
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class MovieControllerTest {

    MockMvc mockMVC;

    @InjectMocks
    MovieController instance;

    @Mock
    MovieService movieService;

    public MovieControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        List<MovieDTO> movieDTOList = new ArrayList<>();
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setActors("Tractors");
        movieDTO.setId(1L);
        movieDTOList.add(movieDTO);
        
        CinemaAndShowDTO cASDTO = new CinemaAndShowDTO();
        CinemaDTO cdto = new CinemaDTO();
        ShowDTO showDTO = new ShowDTO();
        
        cdto.setName("CinemaName");
        cdto.setBase_ticket_price(Integer.MIN_VALUE);
        cdto.setDescription("asd");
        cdto.setEmail("email");
        List<ShowDTO> showsDTOList = new ArrayList();
        showsDTOList.add(showDTO);
        cASDTO.setCinemaDTO(cdto);
        cASDTO.setShowsDTO(showsDTOList);
        List<CinemaAndShowDTO> cASDTOList = new ArrayList<>();
        cASDTOList.add(cASDTO);
        
        
        Mockito.when(movieService.getAllMovies())
                .thenReturn(movieDTOList);
        Mockito.when(movieService.getMovie(1L))
                .thenReturn(movieDTO);
        Mockito.when(movieService.deleteMovie(1L))
                .thenReturn(1L);
        Mockito.when(movieService.addMovie(Mockito.any(MovieDTO.class)))
                .thenReturn(1L);
        Mockito.when(movieService.modifyMovie(Mockito.any(MovieDTO.class)))
                .thenReturn(movieDTO);
        Mockito.when(movieService.reservationHelperGetCinemasAndShows(Mockito.any(Long.class)))
                .thenReturn(cASDTOList);
        Mockito.when(movieService.getTMDBMovie(1L))
                .thenReturn(movieDTO);
        mockMVC = MockMvcBuilders.standaloneSetup(instance).build();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testListOfAllMovies() throws Exception {
        System.out.println("showMovies");
        mockMVC.perform(MockMvcRequestBuilders.get("/movies")).andExpect(MockMvcResultMatchers.jsonPath("$[0].actors").value("Tractors"));
    }

    @Test
    public void testSingleMovie() throws Exception {
        System.out.println("showMovies");
        mockMVC.perform(MockMvcRequestBuilders.get("/movies/1")).andExpect(MockMvcResultMatchers.jsonPath("$.actors").value("Tractors"));
    }

    @Test
    public void testDeleteSingleMovie() throws Exception {
        mockMVC.perform(MockMvcRequestBuilders.delete("/movies/1")).andExpect(MockMvcResultMatchers.jsonPath("$").value("1"));
    }

    @Test
    public void reservationHelperGetCinemasAndShows() throws Exception {
        mockMVC.perform(MockMvcRequestBuilders.get("/movies/{id}/reservation", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content("["
                        + "  {"
                        + "    \"name\": \"CinemaName\""
                        + "  }"
                        + "]"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].cinemaDTO.name").value("CinemaName"));
    }

    @Test
    public void testAddMovie() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MovieDTO movieDTO = new MovieDTO();
        String jsonInString = mapper.writeValueAsString(movieDTO);
        mockMVC.perform(MockMvcRequestBuilders.post("/movies").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andExpect(MockMvcResultMatchers.jsonPath("$").value("1"));
    }

    @Test
    public void testModifyMovie() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MovieDTO movieDTO = new MovieDTO();
        String jsonInString = mapper.writeValueAsString(movieDTO);
        mockMVC.perform(MockMvcRequestBuilders.put("/movies/1").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andExpect(MockMvcResultMatchers.jsonPath("$.actors").value("Tractors"));
    }

    /**
     * Test of getTMDBMovie method, of class MovieController.
     */
    @Test
    public void testGetTMDBMovie() throws Exception {
        mockMVC.perform(MockMvcRequestBuilders.get("/movies/tmdb?id=1")).andExpect(MockMvcResultMatchers.jsonPath("$.actors").value("Tractors"));
    }

}
