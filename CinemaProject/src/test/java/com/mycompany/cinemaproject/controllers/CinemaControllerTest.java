/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.controllers;

import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.datatypes.CinemaDTO;
import com.mycompany.cinemaproject.datatypes.MovieDTO;
import com.mycompany.cinemaproject.datatypes.RoomsDTO;
import com.mycompany.cinemaproject.datatypes.RowsDTO;
import com.mycompany.cinemaproject.services.CinemaService;
import com.mycompany.cinemaproject.services.MovieService;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author SKCADMIN
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class CinemaControllerTest {

    MockMvc moockMvc;

    public CinemaControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        List<CinemaDTO> cinemaDTOList = new ArrayList<>();
        CinemaDTO cinemaDTO = new CinemaDTO();
        cinemaDTO.setName("AladarMozi");
        cinemaDTOList.add(cinemaDTO);

        Mockito.when(cinemaService.returnCinemas())
                .thenReturn(cinemaDTOList);

        Mockito.when(cinemaService.getOneCinema(Mockito.any(Long.class)))
                .thenReturn(cinemaDTO);

        Mockito.when(cinemaService.addNewCinema(Mockito.any(CinemaDTO.class)))
                .thenReturn(cinemaDTO);

        Mockito.when(cinemaService.deleteCinema(Mockito.any(Long.class)))
                .thenReturn(true);

        Mockito.when(cinemaService.modifyCinemas(Mockito.any(Long.class), Mockito.any(CinemaDTO.class)))
                .thenReturn(cinemaDTO);

        RoomsDTO roomDTO = new RoomsDTO();
        roomDTO.setRoomName("Szoba");

        Mockito.when(cinemaService.addNewRoom(Mockito.any(Long.class), Mockito.any(String.class)))
                .thenReturn(roomDTO);

        RowsDTO rowDTO = new RowsDTO();
        rowDTO.setIs_sofa(true);

        Mockito.when(cinemaService.addNewRowToRoom(Mockito.any(Long.class), Mockito.any(RowsDTO.class)))
                .thenReturn(rowDTO);

        List<MovieDTO> movieDTOList = new ArrayList<>();
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setTitle("FilmCim");
        movieDTOList.add(movieDTO);

        Mockito.when(movieservice.getMoviesWhereCinemaShowsThem(Mockito.any(Long.class)))
                .thenReturn(movieDTOList);

        List<RoomsDTO> roomsDTOList = new ArrayList<>();
        RoomsDTO roomsDTO = new RoomsDTO();
        roomsDTO.setRoomName("NameOfTheRoom");
        roomsDTOList.add(roomDTO);

        Mockito.when(cinemaService.showRooms(Mockito.any(Long.class)))
                .thenReturn(roomsDTOList);
        
         Mockito.when(cinemaService.showAllRooms())
                .thenReturn(roomsDTOList);

        moockMvc = MockMvcBuilders.standaloneSetup(instance).build();

    }

    @After
    public void tearDown() {
    }

    @InjectMocks
    CinemaController instance;

    @Mock
    CinemaService cinemaService;

    @Mock
    MovieService movieservice;

    @Test
    public void testShowCinemas() throws Exception {
        System.out.println("showCinemas");
        moockMvc.perform(MockMvcRequestBuilders.get("/cinemas")).andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("AladarMozi"));

    }

    @Test
    public void testAddNewCinema() throws Exception {
        System.out.println("AddNewCinemas");
        moockMvc.perform(MockMvcRequestBuilders.post("/cinemas")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"AladarMozi\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("AladarMozi"));
    }

    @Test
    public void testDeleteCinema() throws Exception {
        System.out.println("deleteCinema");
        moockMvc.perform(MockMvcRequestBuilders.delete("/cinemas/1")).andExpect(MockMvcResultMatchers.jsonPath("$").value(1));
    }

    @Test
    public void testModifyCinemas() throws Exception {
        System.out.println("modifyCinemas");
        moockMvc.perform(MockMvcRequestBuilders.put("/cinemas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"AladarMozi\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("AladarMozi"));
    }

    @Test
    public void testAddNewRoomToCinema() throws Exception {
        System.out.println("addNewRoomToCinema");
        moockMvc.perform(MockMvcRequestBuilders.post("/cinemas/addRoom/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"roomName\":\"Szoba\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roomName").value("Szoba"));
    }

    @Test
    public void testAddNewRowToRoom() throws Exception {
        System.out.println("addNewRowToRoom");
        moockMvc.perform(MockMvcRequestBuilders.post("/cinemas/addRow/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{\"is_sofa\":true}]"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].is_sofa").value(true));
    }

    /**
     * Test of showOneCinema method, of class CinemaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testShowOneCinema() throws Exception {
        System.out.println("showOneCinema");
        moockMvc.perform(MockMvcRequestBuilders.get("/cinemas/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"AladarMozi\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("AladarMozi"));
    }

    /**
     * Test of showCinemaMovies method, of class CinemaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testShowCinemaMovies() throws Exception {
        System.out.println("showCinemaMovies");
        moockMvc.perform(MockMvcRequestBuilders.get("/cinemas/{id}/movies", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"title\":\"FilmCim\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value("FilmCim"));
    }

    /**
     * Test of showCinemasRooms method, of class CinemaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testShowCinemasRooms() throws Exception {
        System.out.println("showCinemasRooms");
        moockMvc.perform(MockMvcRequestBuilders.get("/cinemas/showRooms/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"roomName\":\"NameOfTheRoom\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].roomName").value("Szoba"));
    }

    /**
     * Test of showCinemasAllRooms method, of class CinemaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testShowCinemasAllRooms() throws Exception {
        System.out.println("showCinemasAllRooms");
        moockMvc.perform(MockMvcRequestBuilders.get("/cinemas/showRooms")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"roomName\":\"NameOfTheRoom\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].roomName").value("Szoba"));
    }

}
