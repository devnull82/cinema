/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cinemaproject.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.cinemaproject.TestConfig.TestConfig;
import com.mycompany.cinemaproject.datatypes.UserDTO;
import com.mycompany.cinemaproject.entities.Users;
import com.mycompany.cinemaproject.exceptions.UserAlreadyRegisteredException;
import com.mycompany.cinemaproject.services.UserService;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author Krisz
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class UserControllerTest {
    
    MockMvc mockMVC;

    @InjectMocks
    UserController instance;
    
    @Mock
    UserService userService;
    
    public UserControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MockitoAnnotations.initMocks(this);
         
        Users user = new Users();
         
        Mockito.when(userService.findUser(Mockito.any(UserDTO.class)))
                .thenReturn(user); 
         
        UserDTO uDto = new UserDTO();
        uDto.setEmail("valami@gmail.com");
         
        Mockito.when(userService.addNewUser(Mockito.any(UserDTO.class)))
                .thenReturn(uDto);
        
        mockMVC = MockMvcBuilders.standaloneSetup(instance).build();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of registerUser method, of class UserController.
     */
    @Test
    public void testRegisterUser() throws Exception {
        System.out.println("registerUser");
        //not null case
        ObjectMapper mapper = new ObjectMapper();
        UserDTO uDTO = new UserDTO();
        String jsonInString = mapper.writeValueAsString(uDTO);
        mockMVC.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andExpect(status().isIAmATeapot());
        
        //null case
        Mockito.when(userService.findUser(Mockito.any(UserDTO.class)))
                .thenReturn(null); 
        mockMVC.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andExpect(MockMvcResultMatchers.jsonPath("$.email").value("valami@gmail.com"));
        
    }
    
}
