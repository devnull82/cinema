-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 10.10.1.15    Database: cinemaproject
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `Authorities`
--

LOCK TABLES `Authorities` WRITE;
/*!40000 ALTER TABLE `Authorities` DISABLE KEYS */;
INSERT INTO `Authorities` VALUES (1,'ROLE_USER'),(2,'ROLE_USER'),(3,'ROLE_USER'),(4,'ROLE_USER'),(6,'ROLE_USER');
/*!40000 ALTER TABLE `Authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Authorities_Users`
--

LOCK TABLES `Authorities_Users` WRITE;
/*!40000 ALTER TABLE `Authorities_Users` DISABLE KEYS */;
INSERT INTO `Authorities_Users` VALUES (2,3),(6,7);
/*!40000 ALTER TABLE `Authorities_Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Categories`
--

LOCK TABLES `Categories` WRITE;
/*!40000 ALTER TABLE `Categories` DISABLE KEYS */;
INSERT INTO `Categories` VALUES (1,'Science Fiction'),(2,'Animation'),(3,'Action'),(4,'Western'),(5,'Drama'),(6,'Adventure'),(7,'Thriller'),(8,'Horror'),(9,'Mystery'),(10,'Romance'),(11,'Fantasy');
/*!40000 ALTER TABLE `Categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Cinemas`
--

LOCK TABLES `Cinemas` WRITE;
/*!40000 ALTER TABLE `Cinemas` DISABLE KEYS */;
INSERT INTO `Cinemas` VALUES (3,NULL,NULL,NULL,NULL,47.5,19.04,'mikkamakka',NULL),(7,NULL,NULL,NULL,NULL,47.5,19.03,'VéresHurka',NULL),(8,NULL,NULL,NULL,NULL,47.49,19.034,'MájasHurka',NULL),(9,'sanyi utca 3/b',1000,'De szép a szemed!','sanyi@sanyi.sanyi',47.493,19.035,'Cinema pest','Cinema pest'),(10,'itt',NULL,'the best ever','itt@gmail.hu',47.497,19.032,'CicityCinema',NULL),(11,'1034, Budapest, Próba utca 1',1200,'Nagyon szép és jó!','proba@proba.hu',47.491,19.036,'Cinema Próba','Cinema Próba'),(12,'kjscfkdh',NULL,'khgdkhi','mjbxck',47.495,19.033,'CicaCinema',NULL);
/*!40000 ALTER TABLE `Cinemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Movies`
--

LOCK TABLES `Movies` WRITE;
/*!40000 ALTER TABLE `Movies` DISABLE KEYS */;
INSERT INTO `Movies` VALUES (1,'Mitsuo Iwata, Nozomu Sasaki, Mami Koyama','http://image.tmdb.org/t/p/w500//8Pt7hlu7fkc5IIR0VZ2kNGY8sT4.jpg','Childhood friends Tetsuo and Kaneda are pulled into the post-apocalyptic underworld of Neo-Tokyo and forced to fight for their very survival. Kaneda is a bike gang leader, and Tetsuo is a member of a tough motorcycle crew who becomes involved in a covert government project called Akira. But a bloody battle ensues when Kaneda sets out to save his friend.','Katsuhiro ?tomo','http://image.tmdb.org/t/p/original//wtwn4pSFZhm665cQIHFmBjmgMEC.jpg',5.8,'\0',124,NULL,'1988-07-16 02:00:00','http://image.tmdb.org/t/p/w185//8Pt7hlu7fkc5IIR0VZ2kNGY8sT4.jpg','Akira','https://www.youtube.com/embed/NCmVrn2gnRs'),(2,'Atsuko Tanaka, Akio Ohtsuka, Iemasa Kayumi','http://image.tmdb.org/t/p/w500//hnkvMdSlavMNGadokPD5tImTgrs.jpg','In the year 2029, the barriers of our world have been broken down by the net and by cybernetics, but this brings new vulnerability to humans in the form of brain-hacking. When a highly-wanted hacker known as \'The Puppetmaster\' begins involving them in politics, Section 9, a group of cybernetically enhanced cops, are called in to investigate and stop the Puppetmaster.','Mamoru Oshii','http://image.tmdb.org/t/p/original//jaiaRiiaunJ8faob9vZ8XBTx9ou.jpg',NULL,'\0',83,NULL,'1995-11-18 01:00:00','http://image.tmdb.org/t/p/w185//hnkvMdSlavMNGadokPD5tImTgrs.jpg','Ghost in the Shell','https://www.youtube.com/embed/wNtgA5Xfd2s'),(3,'Leonardo DiCaprio, Tom Hardy, Will Poulter','http://image.tmdb.org/t/p/w500//oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg','In the 1820s, a frontiersman, Hugh Glass, sets out on a path of vengeance against those who left him for dead after a bear mauling.','Alejandro González Iñárritu','http://image.tmdb.org/t/p/original//8jNiQwGgAL5R2xwAcJTGWOZn3Mm.jpg',NULL,'\0',156,NULL,'2015-12-25 01:00:00','http://image.tmdb.org/t/p/w185//oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg','The Revenant','https://www.youtube.com/embed/LoebZZ8K5N0'),(4,'Jamie Foxx, Christoph Waltz, Leonardo DiCaprio','http://image.tmdb.org/t/p/w500//5WJnxuw41sddupf8cwOxYftuvJG.jpg','With the help of a German bounty hunter, a freed slave sets out to rescue his wife from a brutal Mississippi plantation owner.','Quentin Tarantino','http://image.tmdb.org/t/p/original//qUcmEqnzIwlwZxSyTf3WliSfAjJ.jpg',NULL,'\0',165,NULL,'2012-12-25 01:00:00','http://image.tmdb.org/t/p/w185//5WJnxuw41sddupf8cwOxYftuvJG.jpg','Django Unchained','https://www.youtube.com/embed/m-HpQpeE4IY'),(5,'Michael Fassbender, Katherine Waterston, Billy Crudup','http://image.tmdb.org/t/p/w500//ewVHnq4lUiovxBCu64qxq5bT2lu.jpg','Bound for a remote planet on the far side of the galaxy, the crew of the colony ship Covenant discovers what they think is an uncharted paradise, but is actually a dark, dangerous world — whose sole inhabitant is the “synthetic” David, survivor of the doomed Prometheus expedition.','Ridley Scott','http://image.tmdb.org/t/p/original//kMU8trT43p5LFoJ4plIySMOsZ1T.jpg',NULL,'\0',122,NULL,'2017-05-09 02:00:00','http://image.tmdb.org/t/p/w185//ewVHnq4lUiovxBCu64qxq5bT2lu.jpg','Alien: Covenant','https://www.youtube.com/embed/svnAD0TApb8'),(6,'Kurt Russell, Patrick Wilson, Matthew Fox','http://image.tmdb.org/t/p/w500//8A51Ur47D0CNJhmvQlbif0vzyqZ.jpg','During a shootout in a saloon, Sheriff Hunt injures a suspicious stranger. One of the villagers takes care of him in prison. One day they both disappear – only the spear of a cannibal tribe is found. Hunt and a few of his men go in search of the prisoner and his nurse.','S. Craig Zahler','http://image.tmdb.org/t/p/original//eMWYoor3TroQyPw3lNsolnOGqnf.jpg',NULL,'\0',132,NULL,'2015-10-23 02:00:00','http://image.tmdb.org/t/p/w185//8A51Ur47D0CNJhmvQlbif0vzyqZ.jpg','Bone Tomahawk','https://www.youtube.com/embed/0ZbwtHi-KSE'),(10,'Daniel Kaluuya, Allison Williams, Bradley Whitford','http://image.tmdb.org/t/p/w500//1SwAVYpuLj8KsHxllTF8Dt9dSSX.jpg','Chris and his girlfriend Rose go upstate to visit her parents for the weekend. At first, Chris reads the family\'s overly accommodating behavior as nervous attempts to deal with their daughter\'s interracial relationship, but as the weekend progresses, a series of increasingly disturbing discoveries lead him to a truth that he never could have imagined.','Jordan Peele','http://image.tmdb.org/t/p/original//Ae58bf7Yj6OPzwKerPgXSnxCJdh.jpg',NULL,'\0',104,NULL,'2017-02-24 01:00:00','http://image.tmdb.org/t/p/w185//1SwAVYpuLj8KsHxllTF8Dt9dSSX.jpg','Get Out','https://www.youtube.com/embed/sRfnevzM9kQ'),(11,'Keir Dullea, Gary Lockwood, William Sylvester','http://image.tmdb.org/t/p/w500//90T7b2LIrL07ndYQBmSm09yqVEH.jpg','Humanity finds a mysterious object buried beneath the lunar surface and sets off to find its origins with the help of HAL 9000, the world\'s most advanced super computer.','Stanley Kubrick','http://image.tmdb.org/t/p/original//pckdZ29bHj11hBsV3SbVVfmCB6C.jpg',NULL,'\0',149,NULL,'1968-04-05 01:00:00','http://image.tmdb.org/t/p/w185//90T7b2LIrL07ndYQBmSm09yqVEH.jpg','2001: A Space Odyssey','https://www.youtube.com/embed/bAZgLxuBtbo'),(12,'Jared Leto, Diane Kruger, Linh ?an Ph?m','http://image.tmdb.org/t/p/w500//jxd2XSsnUHHUbo50nzVnVnHRPVJ.jpg','Nemo Nobody leads an ordinary existence with his wife and 3 children; one day, he wakes up as a mortal centenarian in the year 2092.','Jaco Van Dormael','http://image.tmdb.org/t/p/original//r9jAdQDK1tYq2wlFPBJBH9UX5TX.jpg',NULL,'\0',156,NULL,'2009-09-11 02:00:00','http://image.tmdb.org/t/p/w185//jxd2XSsnUHHUbo50nzVnVnHRPVJ.jpg','Mr. Nobody','https://www.youtube.com/embed/mpi0qsp3v_w'),(13,'Tom Hanks, Halle Berry, Jim Broadbent','http://image.tmdb.org/t/p/w500//8VNiyIp67ZxhpNgdrwACW0jgvP2.jpg','A set of six nested stories spanning time between the 19th century and a distant post-apocalyptic future. Cloud Atlas explores how the actions and consequences of individual lives impact one another throughout the past, the present and the future. Action, mystery and romance weave through the story as one soul is shaped from a killer into a hero and a single act of kindness ripples across centuries to inspire a revolution in the distant future.  Based on the award winning novel by David Mitchell. Directed by Tom Tykwer and the Wachowskis.','Lilly Wachowski','http://image.tmdb.org/t/p/original//2ZA03KiD4jePTNBTJjGGFTNQPMA.jpg',NULL,'\0',172,NULL,'2012-10-26 02:00:00','http://image.tmdb.org/t/p/w185//8VNiyIp67ZxhpNgdrwACW0jgvP2.jpg','Cloud Atlas','https://www.youtube.com/embed/UmJVFOKaGUY'),(14,'Hugh Jackman, Patrick Stewart, Dafne Keen','http://image.tmdb.org/t/p/w500//9EXnebqbb7dOhONLPV9Tg2oh2KD.jpg','In the near future, a weary Logan cares for an ailing Professor X in a hideout on the Mexican border. But Logan\'s attempts to hide from the world and his legacy are upended when a young mutant arrives, pursued by dark forces.','James Mangold','http://image.tmdb.org/t/p/original//miqmTwqvnYiPEdolZv0biI7vFw2.jpg',NULL,'\0',137,NULL,'2017-02-28 01:00:00','http://image.tmdb.org/t/p/w185//9EXnebqbb7dOhONLPV9Tg2oh2KD.jpg','Logan','https://www.youtube.com/embed/Div0iP65aZo'),(15,'Ellar Coltrane, Patricia Arquette, Ethan Hawke','http://image.tmdb.org/t/p/w500//eKi4e5zXhQKs0De4xu5AAMvu376.jpg','The film tells a story of a divorced couple trying to raise their young son. The story follows the boy for twelve years, from first grade at age 6 through 12th grade at age 17-18, and examines his relationship with his parents as he grows.','Richard Linklater','http://image.tmdb.org/t/p/original//f0HeQdnuFkboXWGBgqtwYUKgLZw.jpg',NULL,'\0',164,NULL,'2014-06-05 02:00:00','http://image.tmdb.org/t/p/w185//eKi4e5zXhQKs0De4xu5AAMvu376.jpg','Boyhood','https://www.youtube.com/embed/ESVgF4P221E');
/*!40000 ALTER TABLE `Movies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Movies_Categories`
--

LOCK TABLES `Movies_Categories` WRITE;
/*!40000 ALTER TABLE `Movies_Categories` DISABLE KEYS */;
INSERT INTO `Movies_Categories` VALUES (1,1),(1,2),(2,3),(2,2),(2,1),(3,4),(3,5),(3,6),(3,7),(4,5),(4,4),(5,8),(5,1),(5,7),(6,8),(6,4),(6,6),(6,5),(10,9),(10,7),(10,8),(11,1),(11,9),(11,6),(12,1),(12,5),(12,10),(12,11),(13,5),(13,1),(14,3),(14,5),(14,1),(15,5);
/*!40000 ALTER TABLE `Movies_Categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Reservations`
--

LOCK TABLES `Reservations` WRITE;
/*!40000 ALTER TABLE `Reservations` DISABLE KEYS */;
/*!40000 ALTER TABLE `Reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Rooms`
--

LOCK TABLES `Rooms` WRITE;
/*!40000 ALTER TABLE `Rooms` DISABLE KEYS */;
INSERT INTO `Rooms` VALUES (1,'Vlm',3),(2,'azokat',3),(3,'néni',7),(4,'Szobaexample1',8),(5,'Szobaexample2',3),(6,'Szobaexample3',7),(7,'Szobaexample4',8),(8,'Szobaexample5',9);
/*!40000 ALTER TABLE `Rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Rows`
--

LOCK TABLES `Rows` WRITE;
/*!40000 ALTER TABLE `Rows` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Shows`
--

LOCK TABLES `Shows` WRITE;
/*!40000 ALTER TABLE `Shows` DISABLE KEYS */;
INSERT INTO `Shows` VALUES (1,'\0','2017-12-25 13:00:00',10,4),(2,'\0','2017-12-25 13:00:00',10,5),(3,'\0','2017-12-26 01:00:00',10,3),(4,'\0','2017-12-25 14:00:00',10,6),(6,'\0','2017-06-13 15:30:00',11,7);
/*!40000 ALTER TABLE `Shows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (3,'attila101710@gmail.com','Teszt','','$2a$10$ShtPE2lxkj/rhURpy9HsH.FnQKJrNMFGDR4jRP5jmdJUUfuqq2m/i',0,'2017-06-09 14:11:45','-86-259114-93-11822-6712182-7912259-109-126-88121-86-28-58-13-8-29424951305046-61098'),(5,'ronie999@gmail.com',NULL,'','$2a$10$Gssk1BegLevA/0.xG04Wb.YZT21tdW3ccdP5H0fAkc44pXwfB8NRK',0,'2017-06-09 15:11:04','-85-1047112311157-12025-127109121-104-1174926-105-9797-10299-5960-782910989-7497-530-44'),(7,'kriszv82@gmail.com',NULL,'','$2a$10$OF2gpa2ts1z3PtqJ6I9l4uXpjnSM7Xowewr4lFxBOUDL4dFih/9Eu',0,'2017-06-12 10:56:02','2-3621-1263111785-89-4431-8-56-2768-8797-803484-467761-541171154884-4050-58-44-30');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-13 13:27:03
